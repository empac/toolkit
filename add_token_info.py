import os
import argparse
import re  # to use regular expressions
import fnmatch  # To match files by pattern
import logging
from tqdm import tqdm

from lxml import etree

logger = logging.getLogger('add token info')
logger.setLevel(logging.INFO)

def add_token_info(indir=None, outdir=None, elements=None, glob_pattern=None):
    input_files = get_files(indir, glob_pattern)
    for i in tqdm(input_files):
        tree = read_xml(i)
        for element in elements:
            items = tree.xpath('//{}'.format(element))
            for item in items:
                text_nodes = item.xpath('.//text()')
                n_tokens = sum([len(x.strip().split('\n')) for x in text_nodes])
                item.attrib['tokens'] = str(n_tokens)
        xml = unprettify(tree)
        filename = os.path.basename(i)
        ofilepath = os.path.join(outdir, filename)
        with open(ofilepath, mode='w', encoding='utf-8') as ofile:
            ofile.write(xml)

def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

def read_xml(infile):
    """Parse the XML file."""
    parser = etree.XMLParser(remove_blank_text=True)
    with open(infile, encoding='utf-8', mode='r') as input:
        return etree.parse(input, parser)


def unprettify(tree):
    """Remove any indentation introduced by pretty print."""
    tree = etree.tostring(  # convert XML tree to string
        tree,
        encoding="utf-8",
        method="xml",
        xml_declaration=True).decode()
    tree = re.sub(  # remove trailing spaces before tag
        r"(\n) +(<)",
        r"\1\2",
        tree)
    tree = re.sub(  # put each XML element in a different line
        r"> *<",
        r">\n<",
        tree)
    tree = re.sub(  # put opening tag and FL output in different lines
        r"(<.+?>)",
        r"\1\n",
        tree)
    tree = re.sub(  # put FL output and closing tag in different liens
        r"(</.+?>)",
        r"\n\1",
        tree)
    tree = re.sub(
        r"(>)([^.])",
        r"\1\n\2",
        tree)
    tree = re.sub(  # remove unnecessary empty lines
        r"\n\n+",
        r"\n",
        tree)
    return tree

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='input',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--elements",
        dest='elements',
        required=False,
        nargs="+",
        choices=['text', 'subtitle', 'line'],
        default=['text', 'subtitle', 'line'],
        help="elements which to add tokens attribute."
    )

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output directory."
    )

    parser.add_argument(
        "--glob",
        dest="glob_pattern",
        required=False,
        default="*.vrt",
        help="glob pattern to select input files."
    )

    args = parser.parse_args()

    indir = args.input
    elements = args.elements
    outdir = args.output
    glob_pattern = args.glob_pattern

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    add_token_info(
        indir=indir,
        outdir=outdir,
        elements=elements,
        glob_pattern=glob_pattern
    )