#!/usr/bin/env bash

#bash download_srt.sh
#bash redownload_srt.sh
bash transform_srt_to_xml.sh
bash pre_treetagger.sh
bash treetagger.sh
bash post_treetagger.sh
bash add_token_info.sh
bash get_sents.sh
bash reencode_corpus.sh
bash from_xml_to_excel.sh
bash describe_corpus.sh
bash get_wrong_segmentation.sh
bash check_which_videos_had_subtitles.sh
bash check_which_files_were_converted.sh
# fix file ownership
sudo chmod -R g+s /data/cqpweb
sudo chown -R www-data:www-data /data/cqpweb/
sudo chmod -R g+s /data/vrt
sudo chown -R ubuntu:ubuntu /data/vrt
sudo chmod -R g+s /data/empac
sudo chmod -R ubuntu:ubuntu /data/empac
