#!/usr/bin/env bash

LANGUAGES=(
en
es
)

YEARS=(
2009
2010
2011
2012
2013
2014
2015
2016
2017
)

## Programme types
TYPES=(
1 # News
2 # Interview
3 # Background
4 # Discovery
5 # History
6 # Others
)

ELEMENT=line

SCRIPT=./wottw/treetagger.py

NORMDIR=/data/empac/corpus/NORM

VRTDIR=/data/empac/corpus/VRT

for language in ${LANGUAGES[@]}
do
    for year in ${YEARS[@]}
    do
        for t in ${TYPES[@]}
        do
            echo "Parsing $language $year $t ...."
            python3 $SCRIPT --input $NORMDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --output $VRTDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --pattern "*.xml" -l $language --tokenize -e $ELEMENT --is_root
        done
    done
done