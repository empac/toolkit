#!/usr/bin/env bash

LANGUAGES=(
en
es
)

SCRIPT=./from_xml_to_excel.py

INDIR=/data/empac/vrt

OUTDIR=/data/empac/data

for language in ${LANGUAGES[@]}
do
    echo "Extracting data from XML to Excel for $language ...."
    python3 $SCRIPT --input $INDIR/$(echo $language | tr "[:lower:]" "[:upper:]") --output $OUTDIR/EMPAC_$(echo $language | tr "[:lower:]" "[:upper:]").xlsx --pattern "*.vrt"
done