import os
import argparse
import fnmatch  # To match files by pattern
import logging
import pandas as pd
import difflib
import re

logger = logging.getLogger('checker')
logger.setLevel(logging.INFO)

def compare_files_in_directories(dir1=None, glob_pattern1=None, dir2=None, glob_pattern2=None, outpath=None):
    """Transform files from SRT format to XML.

    :param indir: input directory for SRT files
    :type indir: str
    :param outpath: output directory to save XML files
    :type outpath: str
    :param glob_pattern: glob pattern to filter files
    :type glob_pattern: str
    """
    input_files1 = get_files(dir1, glob_pattern1)
    input_files2 = get_files(dir2, glob_pattern2)
    # input_files1 = [{'file_path': x, 'basename': get_basename(x)} for x in input_files1]
    # input_files2 = [{'file_path': x, 'basename': get_basename(x)} for x in input_files2]
    df1 = pd.DataFrame()
    df2 = pd.DataFrame()
    df1['file_path'] = input_files1
    df2['file_path'] = input_files2
    df1['basename'] = [get_basename(x) for x in input_files1]
    df2['basename'] = [get_basename(x) for x in input_files2]

    # total_df = pd.merge(df1, df2, on='basename', how='left', suffixes=['_df1', '_df2'], indicator=True)
    not_in_df2, not_in_df1, in_both = compare_file_lists(sorted(df1['basename'].tolist()), sorted(df2['basename'].tolist()))

    # in_both = total_df[total_df['_merge'] == 'both'].drop(columns='_merge')
    # missing_in_df2 = total_df[total_df['_merge'] == 'left_only'].drop(columns='_merge')
    in_both_df = df1[df1.basename.isin(in_both)]
    missing_in_df2 = df1[df1.basename.isin(not_in_df2)]
    writer = pd.ExcelWriter(outpath, engine='xlsxwriter')
    in_both_df.to_excel(writer, sheet_name='in_both', index=False)
    missing_in_df2.to_excel(writer, sheet_name='missing_in_df2', index=False)
    writer.save()

def get_basename(x):
    filename = os.path.basename(x)
    basename = os.path.splitext(filename)[0]
    return basename

def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

def compare_file_lists(list1, list2):
    missing_in_l2 = list()
    missing_in_l1 = list()
    in_both = list()
    diff = difflib.Differ().compare(list1, list2)
    for d in diff:
        if d.startswith('- '):
            missing_in_l2.append(re.sub(r'- ', r'', d))
        elif d.startswith('+'):
            missing_in_l1.append(re.sub(r'\+ ', r'', d))
        elif d.startswith('  '):
            in_both.append(re.sub(r'  ', r'', d))
    return missing_in_l2, missing_in_l1, in_both



if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--dir1",
        dest='dir1',
        required=True,
        help="path to the SRT directory.")

    parser.add_argument(
        "--glob1",
        dest="glob_pattern1",
        required=False,
        default='*.srt',
        help="glob pattern to select input files."
    )

    parser.add_argument(
        "--dir2",
        dest='dir2',
        required=True,
        help="path to the XML directory.")

    parser.add_argument(
        "--glob2",
        dest="glob_pattern2",
        required=False,
        default='*.xml',
        help="glob pattern to select input files."
    )

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output file."
    )

    args = parser.parse_args()

    dir1 = args.dir1
    dir2 = args.dir2
    outpath = args.output
    glob_pattern1 = args.glob_pattern1
    glob_pattern2 = args.glob_pattern2

    outdir = os.path.split(outpath)[0]

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    compare_files_in_directories(
        dir1=dir1,
        glob_pattern1=glob_pattern1,
        dir2=dir2,
        glob_pattern2=glob_pattern2,
        outpath=outpath
    )