#!/usr/bin/env bash

## Input directory
INDIR=/data/empac/corpus/SRT
## Output directory
OUTDIR=/data/empac/corpus/

## Path to script
SCRIPT=./europarltv_redownloader.py

python3 $SCRIPT --input $INDIR --output $OUTDIR --glob "*.xlsx"