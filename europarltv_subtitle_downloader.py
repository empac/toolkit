import argparse
import os
from lxml import etree
import re
import requests
import pandas as pd
import ftfy


programme_types = {
    '1': 'News',
    '2': 'Interview',
    '3': 'Background',
    '4': 'Discovery',
    '5': 'History',
    '6': 'Others'
}

categories = {
    '1': 'EU affairs',
    '2': 'Economy',
    '3': 'Security',
    '4': 'Society',
    '5': 'World',
    '6': 'Others',
    'en': {
        'EU affairs': 'EU affairs',
        'Economy': 'Economy',
        'Security': 'Security',
        'Society': 'Society',
        'World': 'World',
        'Others': 'Others'
    },
    'es': {
        'Asuntos de la UE': 'EU affairs',
        'Economía': 'Economy',
        'Seguridad': 'Security',
        'Sociedad': 'Society',
        'Mundo': 'World',
        'Others': 'Others'
    },
    'de': {
        'EU-Angelegenheiten': 'EU affairs',
        'Wirtschaft': 'Economy',
        'Sicherheit': 'Security',
        'Gesellschaft': 'Society',
        'Welt': 'World',
        'Others': 'Others'
    }
}

def download_subtitles(from_date=None, to_date=None, lang=None, programme_type=None, category=None, url=None, outdir=None):

    url_pattern = url.format(lang)

    if programme_type:
        url_pattern += '&pType={}'.format(programme_type[0])

    if category:
        url_pattern += '&pCategory={}'.format(category[0])

    if from_date:
        url_pattern += '&pDateFrom={}'.format(from_date)

    if to_date:
        url_pattern += '&pDateTo={}'.format(to_date)

    url_pattern += '&sort=date'

    r = requests.get(url_pattern)

    if r.status_code == requests.codes.ok:
        seed = etree.HTML(r.content)
        hits = seed.xpath('//p[@id="showing-records"]')[0]
        if lang == 'en':
            hits = int(re.search(r'of (.+?) videos', hits.text.strip()).group(1).replace(',', ''))
        elif lang == 'es':
            hits = int(re.search(r'de (.+?)$', hits.text.strip()).group(1).replace(',', ''))
        elif lang == 'de':
            hits = int(re.search(r'von (.+?) videos', hits.text.strip()).group(1).replace(',', ''))
        print("Total number of hits: {}".format(hits))
        metadata = list()
        for x in range(1, (hits//8)+2):
            page_url = url_pattern + '&page={}'.format(x)
            print(page_url)
            page = requests.get(page_url)
            page_html = etree.HTML(page.content.decode(encoding='utf-8'))
            results = page_html.xpath('//article[@class="span3 video-item"]')
            for result in results:
                item = dict()
                item['lang'] = lang
                item['title'] = result.xpath('./h1[@class="title"]/a/text()')[0]
                item['video_url'] = 'https://www.europarltv.europa.eu/'+result.xpath('./div/a')[0].attrib['href']
                item['type'] = programme_types[programme_type[0]]
                try:
                    item['category'] = categories[lang][result.xpath('./p[@class="meta"]/a/text()')[0]]
                except:
                    item['category'] = result.xpath('./p[@class="meta"]/a/text()')[0]
                item['date'] = result.xpath('.//p[@class="meta"]/span[@class="date"]/text()')[0].strip()
                try:
                    item['description'] = ftfy.fix_text(result.xpath('./p[2]/text()')[0])
                except:
                    item['description'] = ''
                item['length'] = result.xpath('./div/a/span[1]/text()')[1].strip()
                print(item['video_url'])
                try:
                    video_page = requests.get(item['video_url'])
                    video_html = etree.HTML(video_page.content.decode(encoding='utf-8'))
                    script = video_html.xpath('//script[13]')[0]
                    srt_url = re.search(r'"?srt"?: *"(.+?)"', script.text).group(1)
                    if re.match(r'.+?(-{}X[-_].*|-{}\d*[-_].*|-{}\d*|-{}[A-Z]+)\.SRT$'.format(*[lang.upper()] * 4), os.path.basename(srt_url).upper()):
                        item['srt_url'] = srt_url
                        subtitles = requests.get(srt_url)
                        ofbasename, ofext = os.path.splitext(os.path.basename(srt_url))
                        ofname = ofbasename.upper() + ofext
                        item['id'] = ofbasename.upper()
                        ofpath = os.path.join(outdir, ofname)
                        if ofbasename != 'null':
                            with open(ofpath, mode='wb') as osrt:
                                osrt.write(subtitles.content)
                            print(item['id'])
                            metadata.append(item)
                    else:
                        print("File not processed because language != {}: {}".format(lang.upper(), os.path.basename(srt_url).upper()))
                        item['srt_url'] = srt_url
                        metadata.append(item)
                        continue
                except Exception as e:
                    print(e)
        metadata = pd.DataFrame(metadata)
        mfname = "metadata.xlsx"
        mfpath = os.path.join(outdir, mfname)
        metadata.to_excel(mfpath, index=False)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--from",
        dest='from_date',
        required=True,
        help="from date in format: DD/MM/YYYY")

    parser.add_argument(
        "--to",
        dest='to_date',
        required=False,
        help="to date in format: DD/MM/YYYY")

    parser.add_argument(
        "--lang",
        dest='lang',
        required=True,
        help="language of the files to be downloaded in 2-letter ISO code format."
    )

    parser.add_argument(
        "--url",
        dest='url',
        required=False,
        default="https://www.europarltv.europa.eu/{}/search/?",
        help="base URL for EuroparlTV."
    )

    parser.add_argument(
        "--type",
        dest='programme_type',
        required=False,
        nargs='+',
        choices=['1', '2', '3', '4', '5', '6'],
        help="""type of content.
                1: 'News',
                2: 'Interview',
                3: 'Background',
                4: 'Discovery',
                5: 'History',
                6: 'Others'
                """
    )

    parser.add_argument(
        "--category",
        dest='category',
        required=False,
        nargs='+',
        choices=['1', '2', '3', '4', '5', '6'],
        help="""category of content.
                1: 'EU affairs',
                2: 'Economy',
                3: 'Security',
                4: 'Society',
                5: 'World',
                6: 'Others'
                """
    )

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output directory."
    )

    args = parser.parse_args()

    from_date = args.from_date
    to_date = args.to_date
    lang=args.lang
    programme_type = args.programme_type
    category = args.category
    url = args.url
    outdir = args.output

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    download_subtitles(
        from_date=from_date,
        to_date=to_date,
        lang=lang,
        programme_type=programme_type,
        category=category,
        url=url,
        outdir=outdir)