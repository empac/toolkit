import os
import argparse
import fnmatch  # To match files by pattern
import logging
import difflib
import pandas as pd
from tqdm import tqdm
import copy
from natsort import natsorted


from lxml import etree

logger = logging.getLogger('get alignments')
logger.setLevel(logging.INFO)

def format_list_of_subs_as_not_translated(subs, id, reverse=False):
    """Format list of subtitles as not translated.

    :param subs: list of subtitles
    :type subs: list
    :param id: file ID
    :type id: str
    :param reverse: whether the alignment has to be reversed or not
    :type reverse: bool
    :return: a list of alignments
    """
    output = list()
    if reverse:
        for sub in subs:
            output.append((('',), ["{}:{}".format(id, sub.attrib['no'])]))
    else:
        for sub in subs:
            output.append((("{}:{}".format(id, sub.attrib['no']),), list()))
    return output

def find_overlapping_subtitles(id, subs):
    """Find subtitles with overlapping TCR.

    :param id: file ID
    :type id: str
    :param subs: list of subtitles
    :type subs: list
    :return: a list of subtitles to be aligned, a list of subtitles overlapping with others
    """
    tmp_alignments = {}
    for s1 in subs:
        for s2 in subs:
            if s1.attrib['begin'] <= s2.attrib['begin'] and s1.attrib['end'] >= s2.attrib['end']:
                if '{}:{}'.format(id, s1.attrib['no']) in tmp_alignments:
                    tmp_alignments['{}:{}'.format(id, s1.attrib['no'])]['overlaps'].append('{}:{}'.format(id, s2.attrib['no']))
                else:
                    tmp_alignments['{}:{}'.format(id, s1.attrib['no'])] = {
                        'overlaps': ['{}:{}'.format(id, s2.attrib['no'])]
                    }
    overlaps = [x for x in tmp_alignments if len(tmp_alignments[x]['overlaps']) > 1]
    if overlaps:
        for x in overlaps:
            id, no = x.split(':')
            for idx, s in enumerate(subs):
                if s.attrib['no'] == no:
                    del subs[idx]

    return subs, overlaps

def find_weirdos(id, subs):
    """Find subtitles showing weird properties.

    :param id: file ID
    :type id: str
    :param subs: a list of subtitles
    :type subs: list
    :return: a list of subtitles to be aligned, a list of subtitles showing weird properties
    """
    weirdos = list()
    for s1 in subs:
        if float(s1.attrib['duration']) < 0:  # look for subtitles with negative duration
            weirdos.append('{}:{}'.format(id, s1.attrib['no']))
    if weirdos:
        for x in weirdos:
            id, no = x.split(':')
            for idx, s in enumerate(subs):
                if s.attrib['no'] == no:
                    del subs[idx]

    return subs, weirdos

def align_subtitles(src_subs, tgt_subs, src_id, tgt_id, reverse=False):
    """Align a source and a target list of subtitles by TCR.

    :param src_subs: list of source subtitles
    :param tgt_subs: list of target subtitles
    :param src_id: source text's file ID
    :param tgt_id: target text's file ID
    :param reverse: whether thea alignment has to be reversed or not
    :return: a dictionary of alignments
    """
    alignments = dict()

    src_subs_copy = copy.deepcopy(src_subs)
    tgt_subs_copy = copy.deepcopy(tgt_subs)

    src_subs_copy, src_overlapping = find_overlapping_subtitles(src_id, src_subs_copy)

    if src_overlapping:
        if not reverse:
            for x in src_overlapping:
                alignments[x] = set('')
        else:
            alignments[''] = set(src_overlapping)

    tgt_subs_copy, tgt_overlapping = find_overlapping_subtitles(tgt_id, tgt_subs_copy)

    if tgt_overlapping:
        if not reverse:
            alignments[''] = set(tgt_overlapping)
        else:
            for x in src_overlapping:
                alignments[x] = set('')

    src_subs_copy, src_weirdos = find_weirdos(src_id, src_subs_copy)

    if src_weirdos:
        if not reverse:
            for x in src_weirdos:
                alignments[x] = set('')
        else:
            alignments[''] = set(src_weirdos)

    tgt_subs_copy, tgt_weirdos = find_weirdos(tgt_id, tgt_subs_copy)

    if tgt_weirdos:
        if not reverse:
            for x in tgt_weirdos:
                alignments[x] = set('')
        else:
            alignments[''] = set(tgt_weirdos)

    for s1 in src_subs_copy:

        tmp_alignments = set()
        for s2 in tgt_subs_copy:
            if s1.attrib['begin'] >= s2.attrib['begin'] and s1.attrib['end'] <= s2.attrib['end']:
                tmp_alignments.add("{}:{}".format(tgt_id, s2.attrib['no']))
        if not reverse:
            alignments['{}:{}'.format(src_id, s1.attrib['no'])] = tmp_alignments
        else:
            if tmp_alignments:
                for x in tmp_alignments:
                    if x in alignments:
                        alignments[x].add('{}:{}'.format(src_id, s1.attrib['no']))
                    else:
                        alignments[x] = set(['{}:{}'.format(src_id, s1.attrib['no'])])
            else:
                if '{}:{}'.format(tgt_id, s2.attrib['no']) not in alignments:
                    if '' in alignments:
                        alignments[''].add('{}:{}'.format(tgt_id, s2.attrib['no']))
                    else:
                        alignments[''] = set(['{}:{}'.format(tgt_id, s2.attrib['no'])])

    return alignments


def merge_subtitle_alignments(src_tgt_alignments, tgt_src_alignments):
    """Merge two alignments: typically src to tgt and tgt to src.

    :param src_tgt_alignments: source to target alignments
    :type src_tgt_alignments: dict
    :param tgt_src_alignments: target to source alignments
    :type tgt_src_alignments: dict
    :return: an updated dictionary of alignments
    """
    # update src_tgt with tgt_src
    for k, v in tgt_src_alignments.items():
        if k in src_tgt_alignments:
            src_tgt_alignments[k].update(v)
        elif k == '':
            tmp_v = set()
            for i in v:
                if i not in src_tgt_alignments:
                    tmp_v.add(i)
            if tmp_v:
                if '' in src_tgt_alignments:
                    src_tgt_alignments[''].update(tmp_v)
                else:
                    src_tgt_alignments[''] = tmp_v
        else:
            src_tgt_alignments[k] = v
    return src_tgt_alignments


def merge_segments(alignments):
    """Merge two alignments if they share targets.

    :param alignments: a dictionary of alignments
    :return: an updated list of tuples
    """

    new_alignments = dict()

    # loop to merge segments by shared values
    for k, v in alignments.items():
        seen1 = [x for x in new_alignments if k in x]
        new_k = set()
        new_v = set()
        for k2, v2 in alignments.items():
            if k == k2:
                continue
            if k != k2:
                seen2 = [x for x in new_alignments if k in x]
                if not v.isdisjoint(v2):
                    if len(seen1) == 0:
                        new_v.update(v)
                        new_v.update(v2)
                        new_k.update(set([k, k2]))
                    else:
                        for x in seen1:
                            new_alignments[x].update(v2)
                        for x in seen2:
                            new_alignments[x].update(v2)
        if new_k and new_v:
            new_alignments[tuple(natsorted(new_k))] = new_v
        elif not new_k and not new_v and not seen1:
            new_alignments[(k, )] = v

    # loop to merge segments by shared keys
    for k, v in new_alignments.copy().items():
        new_k = set()
        new_v = set()
        for k2, v2, in new_alignments.copy().items():
            if k == k2:
                continue
            if k != k2:
                if not set(k).isdisjoint(set(k2)):
                    new_k.update(k)
                    new_k.update(k2)
                    new_v.update(v)
                    new_v.update(v2)
                    if k in new_alignments:
                        new_alignments.pop(k)
                    if k2 in new_alignments:
                        new_alignments.pop(k2)
                    new_alignments[tuple(natsorted(new_k))] = new_v

    # loop to sort values
    for k, v in new_alignments.items():
        new_alignments[k] = natsorted(v)

    # loop to sort dictionary
    output = natsorted(new_alignments.items())

    return output

def last_resort_alignment(src_subs, tgt_subs, src_id, tgt_id, alignments):
    """Try to align orphan subtitles.

    :param src_subs: list of source subtitles
    :param tgt_subs: list of target subtitles
    :param src_id: source text's file ID
    :param tgt_id: target text's file ID
    :param alignments: a dictionary of alignments
    :return: an updated dictionary of alignments
    """
    output = dict()
    tgt_orphans = copy.deepcopy(alignments.get(''))
    if tgt_orphans:
        tgt_orphans_no = [x.split(':')[1] for x in tgt_orphans]
    src_orphans_no = [k.split(':')[1] for k,v in alignments.items() if not v]
    if tgt_orphans and src_orphans_no:
        src_orphan_subs = [x for x in src_subs if x.attrib['no'] in src_orphans_no]
        tgt_orphan_subs = [x for x in tgt_subs if x.attrib['no'] in tgt_orphans_no]
        for s1 in src_orphan_subs:
            tmp_alignments = set()
            for s2 in tgt_orphan_subs:
                if s1.attrib['begin'] == s2.attrib['begin'] or s1.attrib['end'] == s2.attrib['end']:
                    tmp_alignments.add("{}:{}".format(tgt_id, s2.attrib['no']))
            output['{}:{}'.format(src_id, s1.attrib['no'])] = tmp_alignments
    # update alignments
    if output:
        for s, t in output.items():
            alignments[s] = t
            if tgt_orphans == t:
                del alignments['']
            else:
                for element in t:
                    tgt_orphans.discard(element)
            alignments[''] = tgt_orphans
    return alignments



def align_by_tcr(src_subs, tgt_subs, src_id, tgt_id):
    """Align the subtitles of two files by their TCR.

    :param src_subs: the list of source subtitles
    :param tgt_subs: the list of target subtitles
    :param src_id: source file ID
    :param tgt_id: target file ID
    :return: a dictionary whose keys are a list of source subtitle IDs, and their values a list of target subtitle IDs
    """
    src_tgt_alignments = align_subtitles(src_subs, tgt_subs, src_id, tgt_id)
    tgt_src_alignments = align_subtitles(tgt_subs, src_subs, tgt_id, src_id, reverse=True)
    alignments = merge_subtitle_alignments(src_tgt_alignments, tgt_src_alignments)
    alignments = last_resort_alignment(src_subs, tgt_subs, src_id, tgt_id, alignments)
    output = merge_segments(alignments)
    return output

def get_alignment_detail(file_alignment, src_id=None, tgt_id=None):
    """Get a detailed log of the alignment of every segment.

    :param file_alignment: the list of segments aligned for two texts
    :type file_alignment: list
    :param src_id: source text's file ID
    :type src_id: str
    :param tgt_id: target text's file ID
    :type tgt_id: str
    :return: a list of segments and their properties
    """
    output = list()
    for segment in file_alignment:
        if segment[0][0] == '':
            len_src = 0
        else:
            len_src = len(segment[0])
        segment_detail = {
            'segment': segment,
            'src_id': src_id,
            'tgt_id': tgt_id,
            'len_src': len_src,
            'len_tgt': len(segment[1])
        }
        output.append(segment_detail)
    return output

def align_files(src_dir, tgt_dir, glob_pattern=None, reverse=False):
    """Align all subtitles in src_dir with corresponding tgt_dir.

    :param src_dir: path to the directory containing source files
    :param tgt_dir: path to the directory containing target files
    :param reverse: if True it returns the dictionary with keys ST segments, and value list of TT segments
    :return: alignment
    """
    src_files = get_files(src_dir, glob_pattern)
    alignments = list()
    alignment_log = list()
    aligned_tgt = list()
    alignments_detail = list()
    for i in tqdm(src_files):
        src_tree = read_xml(i)
        src_root = src_tree.getroot()
        try:
            src_root_record = src_root.attrib['record']
        except:
            # No record for file
            continue
        src_id = src_root.attrib['id']
        tgt_files = get_files(tgt_dir, "{}*{}".format(src_root_record.replace('_', '-'), os.path.splitext(glob_pattern)[1]))
        if len(tgt_files) > 1:
            tgt_file = difflib.get_close_matches(i, tgt_files)[0]
        elif len(tgt_files) == 1:
            tgt_file = tgt_files[0]
        else:
            # No matching files found
            alignment_log.append({'v1': i})
            src_subs = src_tree.xpath('//subtitle')
            file_alignment = format_list_of_subs_as_not_translated(src_subs, src_id)
            alignments += file_alignment
            file_alignment_detail = get_alignment_detail(file_alignment, src_id=src_id)
            alignments_detail += file_alignment_detail
            continue
        alignment_log.append({'v1': i, 'v2': tgt_file})
        tgt_tree = read_xml(tgt_file)
        tgt_root = tgt_tree.getroot()
        tgt_root_record = tgt_root.attrib['record']
        tgt_id = tgt_root.attrib['id']
        if src_root_record == tgt_root_record:
            src_subs = src_tree.xpath('//subtitle')
            tgt_subs = tgt_tree.xpath('//subtitle')
        else:
            # record missmatch
            uri1 = get_uri(src_root.attrib['video_url'])
            uri2 = get_uri(tgt_root.attrib['video_url'])
            if uri1 == uri2:
                src_subs = src_tree.xpath('//subtitle')
                tgt_subs = tgt_tree.xpath('//subtitle')
            else:
                # URI missmatch
                src_subs = src_tree.xpath('//subtitle')
                file_alignment = format_list_of_subs_as_not_translated(src_subs, src_id)
                alignments += file_alignment
                file_alignment_detail = get_alignment_detail(file_alignment, src_id=src_id)
                alignments_detail += file_alignment_detail
                continue
        file_alignment = align_by_tcr(src_subs, tgt_subs, src_id, tgt_id)
        file_alignment_detail = get_alignment_detail(file_alignment, src_id=src_id, tgt_id=tgt_id)
        alignments_detail += file_alignment_detail
        alignments += file_alignment
        aligned_tgt.append(tgt_file)
    tgt_files = get_files(tgt_dir, glob_pattern)
    not_translated_tgt_files = list(set(tgt_files).difference(set(aligned_tgt)))
    # add to alignments empty tgt_files but instead of realign check if already aligned in list of aligned tgt, and that's it.
    for j in tqdm(not_translated_tgt_files):
        alignment_log.append({'v2': j})
        tgt_tree = read_xml(j)
        tgt_root = tgt_tree.getroot()
        tgt_id = tgt_root.attrib['id']
        tgt_subs = tgt_tree.xpath('//subtitle')
        file_alignment = format_list_of_subs_as_not_translated(tgt_subs, tgt_id, reverse=True)
        alignments += file_alignment
        file_alignment_detail = get_alignment_detail(file_alignment, tgt_id=tgt_id)
        alignments_detail += file_alignment_detail
    alignments = alignments_to_string(alignments)
    alignments = "\n".join(alignments)
    with open(outfile, mode='w', encoding='utf-8') as ofile:
        ofile.write(alignments)
    df = pd.DataFrame(alignment_log)
    log_summary = {'aligned': len(df[(df.v1.notnull()) & (df.v2.notnull())]), 'v1_only': len(df[df['v2'].isnull()]),
                   'v2_only': len(df[df['v1'].isnull()])}
    log_summary_df = pd.DataFrame.from_dict(log_summary, orient='index', columns=['videos'])
    writer = pd.ExcelWriter(logfile, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='alignments', index=False)
    log_summary_df.to_excel(writer, sheet_name='summary', index=True)
    df_alignment = pd.DataFrame(alignments_detail)
    df_alignment['segment'] = alignments_to_string(df_alignment['segment'])
    df_alignment.to_excel(writer, sheet_name='segments', index=False)
    writer.save()


def alignments_to_string(alignments):
    """Format as strings the segments of a list of segments.

    :param alignments: a list of the segments
    :return: a list of strings
    """
    output = list()
    for x in alignments:
        src = " ".join([i for i in x[0]])
        tgt = " ".join([i for i in x[1]])
        segment = "{}\t{}".format(src, tgt)
        output.append(segment)
    return output


def get_uri(url):
    uri = os.path.split(url)[1]
    return uri


def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches


def read_xml(infile):
    """Parse the XML file."""
    parser = etree.XMLParser(remove_blank_text=True)
    with open(infile, encoding='utf-8', mode='r') as input:
        return etree.parse(input, parser)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--v1",
        dest='v1',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--v2",
        dest='v2',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output file."
    )

    parser.add_argument(
        "--log",
        dest='logfile',
        required=True,
        help="path to the log file."
    )

    parser.add_argument(
        "--glob",
        dest="glob_pattern",
        required=False,
        default="*.vrt",
        help="glob pattern to select input files."
    )

    args = parser.parse_args()

    src_dir = args.v1
    tgt_dir = args.v2
    outfile = args.output
    logfile = args.logfile
    glob_pattern = args.glob_pattern

    outdir = os.path.split(outfile)[0]

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    logdir = os.path.split(logfile)[0]

    if not os.path.exists(logdir):
        os.makedirs(logdir)

    align_files(
        src_dir=src_dir,
        tgt_dir=tgt_dir,
        glob_pattern=glob_pattern
    )