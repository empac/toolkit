import os
import argparse
import re  # to use regular expressions
import fnmatch  # To match files by pattern
import logging
import nltk
import hashlib
from tqdm import tqdm

from lxml import etree

logger = logging.getLogger('get sentences')
logger.setLevel(logging.INFO)

def get_sentences(indir=None, outdir=None, lang_code=None, glob_pattern=None):
    input_files = get_files(indir, glob_pattern)
    for i in tqdm(input_files):
        tree = read_xml(i)
        text_nodes = tree.xpath('//text()')
        tokens = [x.strip().split('\n') for x in text_nodes]
        tokens = [y for x in tokens for y in x]
        tokens = "\n".join([re.sub(r'^(.+?)\t.+$', r'\1', token) for token in tokens])
        tokenizer = init_tokenizer(lang_code)
        sentences = tokenizer.tokenize(tokens)
        output = etree.Element('text', attrib={'id': tree.getroot().attrib['id'], 'sentences': str(len(sentences))})
        for idx, sentence in enumerate(sentences, start=1):
            n_tokens = len(sentence.split('\n'))
            id = hashlib.md5("_".join((output.attrib['id'], str(idx), sentence)).encode()).hexdigest()
            etree.SubElement(output, 's', attrib={'id': id, 'no': str(idx), 'tokens': str(n_tokens)}).text = sentence
        xml = unprettify(output)
        filename = os.path.basename(i)
        ofilepath = os.path.join(outdir, filename)
        with open(ofilepath, mode='w', encoding='utf-8') as ofile:
            ofile.write(xml)

def process_abbreviations(abbreviation=None):
    """Convert TTG abbreviations to NLTK tokenizer._params.abbrev_types."""
    with open(abbreviation, mode='r', encoding='utf-8') as afile:
        abbreviations = afile.read()
    abbreviations = abbreviations.strip()
    abbreviations = abbreviations.split('\n')
    abbreviations = [re.sub(r'\.$', r'', x) for x in abbreviations]
    abbreviations = [x.lower() for x in abbreviations]
    return abbreviations

def init_tokenizer(lang_code=None, abbreviation=None):
    """Instantiate a tokenizer suitable for the language at stake."""
    lang = {'en': 'english', 'de': 'german', 'es': 'spanish'}
    tokenizer = nltk.data.load(
        'tokenizers/punkt/{}.pickle'.format(lang[lang_code]))
    if abbreviation is not None:
        extra_abbreviations = process_abbreviations(abbreviation)
        tokenizer._params.abbrev_types.update(extra_abbreviations)
    return tokenizer

def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

def read_xml(infile):
    """Parse the XML file."""
    parser = etree.XMLParser(remove_blank_text=True)
    with open(infile, encoding='utf-8', mode='r') as input:
        return etree.parse(input, parser)


def unprettify(tree):
    """Remove any indentation introduced by pretty print."""
    tree = etree.tostring(  # convert XML tree to string
        tree,
        encoding="utf-8",
        method="xml",
        xml_declaration=True).decode()
    tree = re.sub(  # remove trailing spaces before tag
        r"(\n) +(<)",
        r"\1\2",
        tree)
    tree = re.sub(  # put each XML element in a different line
        r"> *<",
        r">\n<",
        tree)
    tree = re.sub(  # put opening tag and FL output in different lines
        r"(<.+?>)",
        r"\1\n",
        tree)
    tree = re.sub(  # put FL output and closing tag in different liens
        r"(</.+?>)",
        r"\n\1",
        tree)
    tree = re.sub(
        r"(>)([^.])",
        r"\1\n\2",
        tree)
    tree = re.sub(  # remove unnecessary empty lines
        r"\n\n+",
        r"\n",
        tree)
    return tree

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='input',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output directory."
    )

    parser.add_argument(
        "--lang",
        dest="lang",
        required=True,
        help="ISO two-letter language code"
    )

    parser.add_argument(
        "--glob",
        dest="glob_pattern",
        required=False,
        default="*.vrt",
        help="glob pattern to select input files."
    )

    args = parser.parse_args()

    indir = args.input
    outdir = args.output
    lang_code = args.lang
    glob_pattern = args.glob_pattern

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    get_sentences(
        indir=indir,
        outdir=outdir,
        lang_code=lang_code,
        glob_pattern=glob_pattern
    )