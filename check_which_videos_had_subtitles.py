import os
import argparse
import fnmatch  # To match files by pattern
import logging
import pandas as pd
from tqdm import tqdm

logger = logging.getLogger('checker')
logger.setLevel(logging.INFO)

def check_which_videos_had_subtitles(indir=None, outpath=None, glob_pattern=None):
    """Transform files from SRT format to XML.

    :param indir: input directory for SRT files
    :type indir: str
    :param outpath: output directory to save XML files
    :type outpath: str
    :param glob_pattern: glob pattern to filter files
    :type glob_pattern: str
    """
    input_files = get_files(indir, glob_pattern)
    total_df = pd.DataFrame()
    for i in tqdm(input_files):
        df = pd.read_excel(i)
        if not df.empty:
            if 'id' in df.columns:
                videos_without_sub = df[pd.isnull(df.id)].copy()
                videos_without_sub['has_srt'] = False
                total_df = total_df.append(videos_without_sub)
                videos_with_sub = df[pd.notnull(df.id)].copy()
                videos_with_sub['has_srt'] = True
                total_df = total_df.append(videos_with_sub)
            else:
                df['id'] = None
                df['has_srt'] = False
                total_df = total_df.append(df)
    total_df['date_as_list'] = total_df['date'].apply(parse_date)
    total_df[['day', 'month', 'year']] = pd.DataFrame(total_df.date_as_list.values.tolist(), index=total_df.index)
    total_df.drop(columns='date_as_list', inplace=True)
    # total_df = total_df[total_df.year < 2018].copy()
    total_df['uri'] = total_df['video_url'].apply(get_uri)
    # total_df.to_excel(outpath, index=False)
    sum_lang = get_summary(total_df, 'videos', ['lang', 'has_srt'])
    sum_year = get_summary(total_df, 'videos', ['lang', 'year', 'has_srt'])
    sum_type = get_summary(total_df, 'videos', ['lang', 'type', 'has_srt'])
    sum_category = get_summary(total_df, 'videos', ['lang', 'category', 'has_srt'])
    sum_year_type = get_summary(total_df, 'videos', ['lang', 'year', 'type', 'has_srt'])
    sum_year_category = get_summary(total_df, 'videos', ['lang', 'year', 'category', 'has_srt'])
    sum_uri = total_df.groupby(['uri', 'has_srt']).size().to_frame('videos')
    sum_lang_uri = get_summary(total_df, 'videos', ['lang', 'has_srt', 'uri'], size=False)
    writer = pd.ExcelWriter(outpath, engine='xlsxwriter')
    total_df.to_excel(writer, sheet_name='all_videos', index=False)
    sum_lang.to_excel(writer, sheet_name='lang')
    sum_year.to_excel(writer, sheet_name='year')
    sum_type.to_excel(writer, sheet_name='type')
    sum_category.to_excel(writer, sheet_name='category')
    sum_year_type.to_excel(writer, sheet_name='year_type')
    sum_year_category.to_excel(writer, sheet_name='year_category')
    sum_uri.to_excel(writer, sheet_name='uri')
    sum_lang_uri.to_excel(writer, sheet_name='lang_uri')

    writer.save()

def get_summary(total_df, freq_var, group_vars, size=True):
    if size:
        df = total_df.groupby(group_vars).size().to_frame(freq_var)
        df['total'] = df.groupby(group_vars[:-1]).transform('sum')
    else:
        df = total_df.groupby(group_vars[:-1])[group_vars[-1]].nunique().to_frame(freq_var)
        df['total'] = df.groupby(group_vars[:-2]).transform('sum')
    df['percent'] = round((df[freq_var] / df['total']) * 100, 2)
    df.drop(columns='total', inplace=True)
    return df

def parse_date(x):
    date, time = x.split(' - ')
    date = date.split('.')
    date = [int(x) for x in date]
    return date

def get_uri(x):
    url, uri = os.path.split(x)
    return uri

def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='input',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output file."
    )

    parser.add_argument(
        "--glob",
        dest="glob_pattern",
        required=False,
        default='*.xlsx',
        help="glob pattern to select input files."
    )

    args = parser.parse_args()

    indir = args.input
    outpath = args.output
    glob_pattern = args.glob_pattern

    outdir = os.path.split(outpath)[0]

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    check_which_videos_had_subtitles(
        indir=indir,
        outpath=outpath,
        glob_pattern=glob_pattern
    )