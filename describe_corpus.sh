#!/usr/bin/env bash

LANGUAGES=(
en
es
)

SCRIPT=./describe_corpus.py

INDIR=/data/empac/data

OUTDIR=/data/empac/data

for language in ${LANGUAGES[@]}
do
    echo "Describing corpus for $language ...."
    python3 $SCRIPT --input $INDIR/EMPAC_$(echo $language | tr "[:lower:]" "[:upper:]")\_text.xlsx --output $OUTDIR/EMPAC_$(echo $language | tr "[:lower:]" "[:upper:]")\_description.xlsx
done