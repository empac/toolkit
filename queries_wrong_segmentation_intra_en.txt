# Prep | PREP
[pos="IN"]</line><line_no="2">[pos="DT|PP$"]*[pos="J.+|RB|N.+"]*[pos="N.+"];
# Det_Indef | DET
[pos="DT" & lemma="a"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="N.+"];
# Det_Def | DET
[pos="DT" & lemma="the"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="N.+"];
# Det_Dem | DET
[pos="DT" & lemma="this|that|these|those"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="N.+"];
# Det_Pos | DET
[pos="PP$"]</line><line_no="2">[]
# Verb_Aux | VERB_COMP
[pos="VB.+|VD.+|VH.+|MD.+"]</line><line_no="2">[pos="VV.+|VB.+|VD.+|VH.+"]
# Verb_Inf | VERB_COMP
[pos="TO"]</line><line_no="2">[pos="VB|VD|VH|VV"]
# Verb_Phrasal | VERB_COMP
[pos="VV.*|VB.*|VH.*|VD.*"][pos="RP+|IN+"]*</line><line_no="2">[pos="RP+|IN+"]*[pos="RP+|IN+"]+;
# Conj | CONJ
[pos="CC"]</line><line_no="2">
# Frase | FRASE
[]</s><s>[] within line;
# Sintag_Adj | SINT
[pos="PP$|DT"] [pos="J.+"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="J.+|N.+"];
# Sintag_Adv | SINT
[pos="WRB|RB"]</line><line_no="2">[pos="J."]*[pos="V.+|N."];
# Nom_Comp | NOM_COMP
[pos="NNS?|NPS?|NNS?" & word!="%"]+</line><line_no="2">[pos="NNS?|NPS?|NNS?"]+;