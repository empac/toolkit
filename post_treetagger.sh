#!/usr/bin/env bash

LANGUAGES=(
en
es
)

YEARS=(
2009
2010
2011
2012
2013
2014
2015
2016
2017
)

## Programme types
TYPES=(
1 # News
2 # Interview
3 # Background
4 # Discovery
5 # History
6 # Others
)

TEXTCONTAINER=line

SCRIPT=./wottw/post_treetagger.py

INDIR=/data/empac/corpus/VRT

OUTDIR=/data/empac/corpus/VRT

for language in ${LANGUAGES[@]}
do
    for year in ${YEARS[@]}
    do
        for t in ${TYPES[@]}
        do
            echo "Postprocessing TreeTagger output for $language $year $t ...."
            python3 $SCRIPT --input $INDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --output $OUTDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --glob_pattern "*.vrt"  --lang $language --text line
        done
    done
done
