# empac_utils

Utilities to process the EMPAC corpus.

## Description

The EMPAC corpus is made up of subtitles from the contents distributed through the [Multimedia Centre of the European Parliament](https://multimedia.europarl.europa.eu), formerly known as EuroParlTV. These utilities are devised to compile versions in English and Spanish and to align them at document and subtitle level. See the section on [EMPAC's compilation](empac-processing-pipeline) for a detailed description.

## Repository contents

```text
├── README.md
├── add_token_info.py
├── add_token_info.sh
├── check_which_files_were_converted.py
├── check_which_videos_had_subtitles.py
├── check_which_videos_had_subtitles.sh
├── compare_files_in_directories.py
├── describe_corpus.py
├── describe_corpus.sh
├── download_srt.sh
├── europarltv_redownloader.py
├── europarltv_subtitle_downloader.py
├── from_srt_to_xml.py
├── from_xml_to_excel.py
├── from_xml_to_excel.sh
├── get_alignments.py
├── get_sentences.py
├── get_sents.sh
├── get wrong_segmentation.py
├── get wrong_segmentation.sh
├── pipeline.sh
├── pre_treetagger.sh
├── queries_wrong_segmentation_inter_en.txt
├── queries_wrong_segmentation_inter_es.txt
├── queries_normalization_inter.txt
├── queries_wrong_segmentation_intra_en.txt
├── queries_wrong_segmentation_intra_es.txt
├── queries_normalization_intra.txt
├── redownload_srt.sh
├── reencode_corpus.sh
├── requirements.txt
├── transform_srt_to_xml.sh
└── treetagger.sh
```


## EMPAC processing pipeline

1. Download SRT files
1. Model subtitles as XML
1. Annotate linguistic information with TreeTagger
1. Annotate sentence boundaries
1. Encode the corpus for CQPweb and align subtitles
1. Extract texts' metadata from XML to Excel
1. Upload the corpus to CQPweb server
1. Segmentation query design with CQPweb
1. Segmentation feature extraction
1. Corpus description

### Download SRT files

#### Download from search queries

If SRT files are to be crawled for first time use `europarltv_subtitle_downloader.py`. See an example of its usage in `download_srt.sh`. 

This script:

1. builds a query to search in the repository of the Multimedia Centre given several parameters;
1. paginates the results;
1. retrieves the URLs of the videos and other metadata;
1. downloads the subtitles in SRT format;
1. saves the metadata as an Excel spreadsheet.

The parameters that can be passed to restrict the query are:

- `from`: begin date for the period one wants to crawl.
- `to`: end date for the period one wants to crawl.
- `lang`: language of the subtitles.
- `url`: where to launch the search query (`https://www.europarltv.europa.eu/{}/search/?`)
- `type`: type of programme (loosely corresponding to register), namely
    - News
    - Interview
    - Background
    - Discovery
    - History
    - Others
- `category`: categorization of contents as used by the source (loosely corresponding to field/domain), namely
    - EU affairs
    - Economy
    - Security
    - Society
    - World
    - Others
    

The metadata retrieved at this stage contain the following information:

- `lang`: the language of the subtitle in two-letter ISO code (eg. `en` for English);
- `title`: the title of the video;
- `video_url`: the URL of the audiovisual text;
- `type`: the type of programme;
- `category`: the category of the audiovisual text;
- `date`: the date when the audiovisual text was (re-)published in the platform;
- `description`: a brief description/summary of the contents of the text;
- `length`: the duration of the audiovisual text in time in the format HH:MM:SS;
- `srt_url`: the URL of the SRT file;
- `id`: a unique ID based on the name of the SRT file, it must be a valid MySQL handle



#### Download from a list of local files

If files were already downloaded at least once, one can use the list of files to download again the SRT files skipping the time consuming requests of result pages of the search interface of the Multimedia Centre of the European Parliament and thus saving precious time.

Use `europarltv_redownloader.py` as illustrated in `redownload_srt.sh`.

### Model subtitles as XML

SRT files are transformed to XML and several features are calculated with `from_srt_to_xml.py`.

This script does the following:

- loads the metadata file generated during the download stage;
- loops over all SRT files located in the input folder;
- checks that the language of the file is right
- parses the information of the file as SRT to create a list of subtitles, identifying:
    - subtitle
    - subtitle number
    - begin time
    - end time
    - duration
    - lines
    - number of lines
    - characters per subtitle
    - characters per line
    - characters per second
    - pause between current subtitle and previous one
- generates `XML` adding information:
    - at text level from metadata:
        - id
        - category
        - type
        - record
        - version
        - date
        - day
        - month
        - year
        - number of subtitles
        - duration
        - number of one-liners
        - number of two-liners
        - number of n-liners
    - at subtitle level:
        - id
        - number/position
        - begin
        - end
        - duration
        - number of lines
        - number of characters
        - characters per second
        - pause
    - at line level:
        - id
        - line number/position
        - number of characters
        - text

Takes as arguments:

- `input`: path to the input folder containing the `SRT` files.
- `metadata`: path to the Excel file containing the metadata.
- `output`: path to the folder where `XML` files will be saved
- `lang`: language in two-letter ISO code format.
- `glob`: a glob pattern to select input files.

The `SRT` input is:

```text
1
00:00:02,480 --> 00:00:04,760
SEPA is the Single Euro Payments Area.

2
00:00:04,920 --> 00:00:08,560
If you find making payments across Europe
risky, expensive and slow.
```

The output is:

```xml
<?xml version='1.0' encoding='utf-8'?>
<text category="Others" date="2011-12-21 14:59:00" day="21" description="Fraud and hidden charges should be a thing of the past when making EU cross-border payments in a new deal on the Single Euro Payment Area. And more of today's news." duration="00:02:55" id="N001_111221_EN_MF" lang="en" month="12" n_liners="0" n_lines="73" one_liners="7" record="N001-111221" srt_url="https://www.europarltv.europa.eu/programme-files/2777/srt/n001-111221-en_mf.srt" subtitles="40" title="The News: Payments without borders" two_liners="33" type="News" version="" video_url="https://www.europarltv.europa.eu//programme/others/the-news-payments-without-borders" year="2011">
  <subtitle begin="00:00:02,480" chars="38" cps="18.97" duration="2.003" end="00:00:04,760" id="7f53d31b32e80f9ace77d1bcdc2b36fa" n_lines="1" no="1" pause="0">
    <line chars="38" id="fcbc0fb2f6eccdd9e07db2d01a177938" no="1">SEPA is the Single Euro Payments Area.</line>
  </subtitle>
  <subtitle begin="00:00:04,920" chars="67" cps="16.77" duration="3.996" end="00:00:08,560" id="4be0f79232c4ff77f258ec00b2b00651" n_lines="2" no="2" pause="0.002">
    <line chars="41" id="4c0662a14ead2fec5459bb5ef6384044" no="1">If you find making payments across Europe</line>
    <line chars="26" id="864339894e6cb9b2855d3408200c52ce" no="2">risky, expensive and slow.</line>
  </subtitle>
  ...
</text>
```

`transform_srt_to_xml.sh` illustrates the usage of this script.

### Anotate linguistic information

The linguistic annotation of the subtitles is carried out in two steps using the utilities of the [wrapper of the TreeTagger wrapper](https://github.com/chozelinek/wottw), which in turn use [mytreetaggerwrapper](https://github.com/chozelinek/mytreetaggerwrapper), and the [TreeTagger](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger):

1. preprocessing with `pre_treetagger.py`
1. actual linguistic annotation with TreeTagger with `treetagger.py`
1. postprocessing with `post_treetagger.py`
1. enrichment of XML elements with token information with `add_token_info.py`

#### Preprocessing

Before annotating linguistic information with the TreeTagger, XML files are preprocessed with `pre_treetagger.py` which is an utility from the [wrapper of the TreeTagger wrapper](https://github.com/chozelinek/wottw). This script performs character normalization on XML text content to obtain better results with TreeTagger.

You can see an example of its usage in `pre_treetagger.sh`. 

#### Linguistic annotation

Linguistic annotation is obtained using `treetagger.py`. This script is a Python interface to annotate text contained in an XML file with linguistic information using the TreeTagger.

An example to annotate XML files can be found in `treetagger.sh`.

After this process the texts are in VRT format as expected by the Open Corpus Workbench.

VRT format is XML and text is represented as one token per line, and each line, separated by tabs, word form, PoS, and lemma.

#### Postprocessing

After annotating linguistic information with the TreeTagger, XML files are postprocessed with `post_treetagger.py` which is an utility from the [wrapper of the TreeTagger wrapper](https://github.com/chozelinek/wottw). This script performs character normalization on TreeTagger output to fix some issues.

You can see an example of its usage in `post_treetagger.sh`. 

#### Token information

Token information (number of `tokens`) is added at three levels with `add_token_info.py`:

- text
- subtitle
- lines

Its usage is illustrated in `add_token_info.sh`.

A sample of the final output annotated with linguistic information:

```xml
<?xml version='1.0' encoding='utf-8'?>
<text category="Others" date="2011-12-21 14:59:00" day="21" description="Fraud and hidden charges should be a thing of the past when making EU cross-border payments in a new deal on the Single Euro Payment Area. And more of today's news." duration="00:02:55" id="N001_111221_EN_MF" lang="en" month="12" n_liners="0" n_lines="73" one_liners="7" record="N001-111221" srt_url="https://www.europarltv.europa.eu/programme-files/2777/srt/n001-111221-en_mf.srt" subtitles="40" title="The News: Payments without borders" two_liners="33" type="News" version="" video_url="https://www.europarltv.europa.eu//programme/others/the-news-payments-without-borders" year="2011" tokens="471">
<subtitle begin="00:00:02,480" chars="38" cps="18.97" duration="2.003" end="00:00:04,760" id="7f53d31b32e80f9ace77d1bcdc2b36fa" n_lines="1" no="1" pause="0" tokens="8">
<line chars="38" id="fcbc0fb2f6eccdd9e07db2d01a177938" no="1" tokens="8">
SEPA	NP	SEPA
is	VBZ	be
the	DT	the
Single	NP	Single
Euro	NP	Euro
Payments	NNS	payment
Area	NP	Area
.	SENT	.
</line>
</subtitle>
<subtitle begin="00:00:04,920" chars="67" cps="16.77" duration="3.996" end="00:00:08,560" id="4be0f79232c4ff77f258ec00b2b00651" n_lines="2" no="2" pause="0.002" tokens="13">
<line chars="41" id="4c0662a14ead2fec5459bb5ef6384044" no="1" tokens="7">
If	IN	if
you	PP	you
find	VVP	find
making	VVG	make
payments	NNS	payment
across	IN	across
Europe	NP	Europe
</line>
<line chars="26" id="864339894e6cb9b2855d3408200c52ce" no="2" tokens="6">
risky	JJ	risky
,	,	,
expensive	JJ	expensive
and	CC	and
slow	JJ	slow
.	SENT	.
</line>
</subtitle>
...
</tex>
```

### Annotate sentence boundaries

Sentence boundaries are annotated separately in a different XML file. Tokenization is the same as in VRT files produced in previous steps. This enables multi-layer stand-off annotation granting well-formed valid XML.

Sentence annotation is carried out using `get_sentences.py`. It uses the `punkt` sentence tokenizer of the [NLTK](http://www.nltk.org).

The output is an VRT file with the same token stream produced by TreeTagger.

An example of its usage can be found in `get_sents.sh`.

An example of the output:

```xml
<?xml version='1.0' encoding='utf-8'?>
<text id="N001_111221_EN_MF" sentences="25">
<s id="d5d2007f70ea5399d07d0f80c8a2465e" no="1" tokens="8">
SEPA
is
the
Single
Euro
Payments
Area
.
</s>
<s id="1d384ff73b04c76c02c95c2cb78567a5" no="2" tokens="13">
If
you
find
making
payments
across
Europe
risky
,
expensive
and
slow
.
</s>
...
</text>
```

### Encode the corpus for CQPweb and align subtitles

The script `reencode_corpus.sh` does the encoding of the corpus with the Open Corpus WorkBench toolkit, generates the alignment from the VRT files with the script `get_alignments.py`, and imports the alignments with the Open Corpus WorkBench utilities.

`reencode_corpus.sh` gathers together in the same folder or VRT files with texts' metadata, text structure, and the linguistic information at token level, the sentence boundaries. All this information is indexed with `cwb-encode` to enable CQP queries.

Then, it generates the alignments for each pair of languages using `get_alignment.py`. This script takes as input the following parameters:

- `v1`: path to the folder containing the texts of V1;
- `v2`: path to the folder containing the texts of V2;
- `output`: path to the folder where the output should be saved;
- `att1`: an structural attribute whose value is used to compose unique subtitle IDs (in our case `text_id`);
- `att2`: an structural attribute whose value is used to compose unique subtitle IDs (in our case `subtitle_no`);
- `glob`: a globbing pattern to filter input files.

For each file in V1 it finds its counterpart in V2. If a matching file is found, the number of subtitles is compared. Normally, the number of subtitles tend to be the same, if that's the case a one to one alignment of subtitles is assumed and for each subtitle a string is generated with the structural element IDs. If there is a missmatch regarding the number of subtitles, the files are aligned using the time code (TCR). Finally a plain text file with the alignment information is produced.

Example of an alignment file:

```text
N001_100420_010_EN:1	N001_100420_010_ES:1
N001_100420_010_EN:2	N001_100420_010_ES:2
N001_100420_010_EN:3	N001_100420_010_ES:3
N001_100420_010_EN:4	N001_100420_010_ES:4
N001_100420_010_EN:5	N001_100420_010_ES:5
N001_100420_010_EN:6	N001_100420_010_ES:6
N001_100420_010_EN:7	N001_100420_010_ES:7
N001_100420_010_EN:8	N001_100420_010_ES:8
N001_100420_010_EN:9	N001_100420_010_ES:9
N001_100420_010_EN:10	N001_100420_010_ES:10
```

The alignments are incorporated into the indexed corpus using the utility `cwb-align-import` in both directions.

This enables both the visualization of a subtitle and its aligned version(s) in other languages, and parallel queries.

### Extract texts' metadata from XML to Excel

The VRT files contain metatextual information at text, subtitle and level. `from_xml_to_excel.py` this information is extracted and saved as an Excel file.

The script takes three parameters:

- `input`, the path to the directory containing the VRT files;
- `output`, the path where to save the resulting Excel file; and
- `pattern`, a glob pattern to filter input files.

The script creates three empty dataframes:

1. one for information at subtitle level
1. one for information at text level
1. one for information at line level

For each file matching the pattern found in the input directory it gets the metadata at each of those levels and adds the records for that file to each of the dataframes.

Finally, the output is produced, an Excel dataframe with four spreadsheets:

- texts: containing information about the texts only, each row represents a text.
- subtitles: containing information about the subtitles and the texts, each row represents a subtitle.
- lines: containing information about the lines, the subtitles, and the texts, each row represents a line.

An example of its usage can be found in `from_xml_to_excel.sh`.

#### Information extracted about texts

Each row represents a text, each column a field describing the text:

- `text_id`: unique ID of the text
- `record`: the first part of the ID that identifies the document across languages
- `version`: the second part of the ID denoting the version of the document, mostly referring to the language
- `lang`: the language of the subtitle, ISO two-letter code
- `type`: type of programme/register (News, Interview, Background, Discovery, History, Others) 
- `category`: field/domain (EU affairs, Economy, Security, Society, World, Others)
- `year`: year of publication
- `month`: month of publication
- `day`: day of publication
- `date`: date and time of publication in ISO format 
- `duration`: in HH:MM:SS
- `subtitles`: number of subtitles in text
- `n_lines`: number of lines
- `one_liners`: number of subtitles made up of one line
- `two_liners`: number of subtitles made up of two lines
- `n_liners`: number of subtitles madep up of more than two lines
- `tokens`: number of tokens in text
- `video_url`: URL to the video
- `srt_url`: URL to the SRT file
- `description`: brief abstract as provided by the source

#### Information extracted about subtitles

Each row represents a subtitle of the corpus, first the same columns as above with information about the text are given, then each column is a field describing a feature of the subtitle. Suffixes are used to distinguish to which level a column refers to, if the value is given for both text `_t` and subtitle `_s`.

- `subtitle_id`: unique ID of the subtitle (a hash, it has no semantics)
- `no`: number/position in the text of the subtitle, count starts at 1.
- `begin`: time code for the beginning of the subtitle
- `end`: time code for the end of the subtitle
- `duration_s`: duration of the display of the subtitle
- `chars`: number of characters in subtitle
- `cps`: characters per second
- `n_lines_s`: number of lines in subtitle
- `tokens_s`: number of tokens in subtitle
- `pause`: elapsed time in seconds between the current subtitle and the preceding one.

#### Information extracted about lines

Each row represents a line of the corpus, first we found the same information as above, text and subtitle, then each column is a field describing a feature of the line. Suffixes are used to distinguish to which level a column referst to, if the value is given for text `_t`, subtitle `_s` and line `_l`.

- `line_id`: unique ID of th eline (a hash, it has no semantics)
- `no_l`: number/position of line in the subtitle, count starts at 1.
- `chars_l`: number of characters in line
- `tokens_l`: number of tokens in line

### Segmentation query design with CQPweb

CQPweb is used to design a set of queries to extract intra- and inter-subtitle segmentation features.

The result is gathered in the following files:

- `queries_wrong_segmentation_inter_en.txt`, for inter-subtitle segmentation in English
- `queries_wrong_segmentation_inter_es.txt`, for inter-subtitle segmentation in Spanish
- `queries_normalization_inter.txt`, to normalize the results for inter-subtitle segmentation for subcorpus comparison and to get the proportion of erroneous segmentations
- `queries_wrong_segmentation_intra_en.txt`, for intra-subtitle segmentation in English
- `queries_wrong_segmentation_intra_es.txt`, for intra-subtitle segementation in Spanish
- `queries_normalization_intra.txt`, to normalize the results for intra-subtitle segmentation for subcorpus comparison and to get the proportion of erroneous segmentations


### Segmentation feature extraction

Segmentation queries are run with the script `get_wrong_segmentation.py`. This script runs the queries on a corpus, and across subcorpora, and normalizes the results.

An example of its usage can be found in `get_wrong_segmentation.sh`. 

The script can take the following parameters:

- `corpus`: the name of the corpus as in the registry for CQP. In our case: `EMPAC_EN` or `EMPAC_ES`.
- `subcorpus`: the variable for which we will get the distribution of the results (`year`).
- `output`: the path the file to save the output.
- `queries`: a plain text file containing the queries to retrieve wrong segmentations.
- `normalization`: a plain text file containing the query to normalize the results.
- `registry`: the path to the CWB registry.
- `cqp`: the path to the `cqp` binary.

The queries document is a plain text file, if the line starts with a `#` symbol, it contains the name of the feature and the category of analysis. The following line contains the string for the query in `CQP` language.

Example of a query file:

```bash
# Prep | PREP
[pos="IN"]</line><line_no="2">[pos="DT|PP$"]*[pos="J.+|RB|N.+"]*[pos="N.+"];
# Det_Indef | DET
[pos="DT" & lemma="a"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="N.+"];
# Det_Def | DET
[pos="DT" & lemma="the"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="N.+"];
# Det_Dem | DET
[pos="DT" & lemma="this|that|these|those"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="N.+"];
# Det_Pos | DET
[pos="PP$"]</line><line_no="2">[]
# Verb_Aux | VERB_COMP
[pos="VB.+|VD.+|VH.+|MD.+"]</line><line_no="2">[pos="VV.+|VB.+|VD.+|VH.+"]
# Verb_Inf | VERB_COMP
[pos="TO"]</line><line_no="2">[pos="VB|VD|VH|VV"]
# Verb_Phrasal | VERB_COMP
[pos="VV.*|VB.*|VH.*|VD.*"][pos="RP+|IN+"]*</line><line_no="2">[pos="RP+|IN+"]*[pos="RP+|IN+"]+;
# Conj | CONJ
[pos="CC"]</line><line_no="2">
# Frase | FRASE
[]</s><s>[] within line;
# Sintag_Adj | SINT
[pos="PP$|DT"] [pos="J.+"]</line><line_no="2">[pos="J.+|N.+|RB"]*[pos="J.+|N.+"];
# Sintag_Adv | SINT
[pos="WRB|RB"]</line><line_no="2">[pos="J."]*[pos="V.+|N."];
# Nom_Comp | NOM_COMP
[pos="NNS?|NPS?|NNS?" & word!="%"]+</line><line_no="2">[pos="NNS?|NPS?|NNS?"]+;
```

The output is an Excel file with several tabs:

- `feature_total`, the distribution of wrong segmentations by features/queries for the corpus as a whole
- `feature_subcorpus_long`, the distribution of wrong segmentations by features/queries across all subcorpora in long format.
- `feature_subcorpus_freq`, the distribution of wrong segmentations as absolute frequencies by features/queries across all subcorpora in wide format.
- `feature_subcorpus_rel_freq`, the distribution of wrong segmentations as relative frequencies (normalized) by features/queries across all subcorpora in wide format.
- `category_total`, the distribution of wrong segmentations by categories for the corpus as a whole
- `category_subcorpus_long`, the distribution of wrong segmentations by categories across all subcorpora in long format.
- `category_subcorpus_freq`, the distribution of wrong segmentations as absolute frequencies by categories across all subcorpora in wide format.
- `category_subcorpus_rel_freq`, the distribution of wrong segmentations as relative frequencies (normalized) by categories across all subcorpora in wide format.


Example of output, category total:

category | freq | total | rel_freq
---|---|---|---
CONJ | 1302 | 170698 | 7,627505888
DET | 2503 | 170698 | 14,66332353
FRASE | 6410 | 170698 | 37,55169949
NOM_COMP | 3870 | 170698 | 22,67161888
PREP | 6164 | 170698 | 36,11055783
SINT | 4358 | 170698 | 25,53046902
VERB_COMP | 9036 | 170698 | 52,93559386

Example of output, category subcorpus long:

category | year | freq | total | rel_freq
---|---|---|---|---
CONJ | 2009 | 142 | 17521 | 8,104560242
CONJ | 2010 | 328 | 36608 | 8,95979021
CONJ | 2011 | 195 | 27346 | 7,130841805
CONJ | 2012 | 218 | 28954 | 7,529184223
CONJ | 2013 | 176 | 26464 | 6,650544135
CONJ | 2014 | 91 | 17079 | 5,328180807
CONJ | 2015 | 93 | 10922 | 8,514924007
CONJ | 2016 | 53 | 5173 | 10,24550551
CONJ | 2017 | 6 | 631 | 9,508716323
DET | 2009 | 349 | 17521 | 19,9189544
DET | 2010 | 728 | 36608 | 19,88636364
DET | 2011 | 336 | 27346 | 12,28698896
DET | 2012 | 286 | 28954 | 9,8777371
DET | 2013 | 257 | 26464 | 9,711305925
DET | 2014 | 231 | 17079 | 13,52538205
DET | 2015 | 183 | 10922 | 16,75517305
DET | 2016 | 107 | 5173 | 20,68432244
DET | 2017 | 26 | 631 | 41,2044374

Example of output, category subcorpus freq:

category | 2009 | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 | 2016 | 2017
---|---|---|---|---|---|---|---|---|---
CONJ | 142 | 328 | 195 | 218 | 176 | 91 | 93 | 53 | 6
DET | 349 | 728 | 336 | 286 | 257 | 231 | 183 | 107 | 26
FRASE | 613 | 1028 | 837 | 1544 | 1415 | 533 | 286 | 104 | 50
NOM_COMP | 380 | 886 | 568 | 644 | 568 | 383 | 282 | 137 | 22
PREP | 574 | 1837 | 1176 | 950 | 709 | 363 | 359 | 171 | 25
SINT | 553 | 911 | 698 | 703 | 638 | 440 | 242 | 144 | 29
VERB_COMP | 962 | 1776 | 1439 | 1563 | 1457 | 879 | 604 | 305 | 51

### Corpus description

#### General description

The corpus is described with the script `describe_corpus.py`. This script generates a summary of the corpus in terms of size in texts, size in subtitles, size in tokens and duration in hours, by language, year, type, category, year and type, and, year and category.

The script can take the following parameters:

- `input`: the path to the excel file containing the information to be summarized.
- `output`: the path the file to save the output.

An example of its usage can be found in `describe_corpus.sh`.

The output is an Excel file with several tabs:

- `total`, the description for the corpus as a whole
- `year`, the description across all years
- `type`, the description across all types
- `category`, the description across all categories
- `year_type`, the description across all years and types
- `year_category`, the description across all years and categories

Example of output, by years:

year | texts | subtitles | tokens | duration
---|---|---|---|---
2009 | 355 | 23120 | 247647 | 40:08:21
2010 | 744 | 48533 | 510721 | 57:18:52
2011 | 563 | 34054 | 372787 | 39:53:46
2012 | 592 | 37392 | 417046 | 43:17:27
2013 | 484 | 34704 | 385908 | 38:57:59
2014 | 356 | 21532 | 244466 | 25:04:52
2015 | 361 | 13939 | 156020 | 16:34:16
2016 | 203 | 6996 | 77264 | 8:16:33
2017 | 154 | 3766 | 36913 | 4:11:04

Example of output, by years and types:

year | type | texts | subtitles | tokens | duration
---|---|---|---|---|---
2009 | Background | 100 | 10688 | 113795 | 23:28:36
2009 | Discovery | 49 | 1665 | 17854 | 2:15:21
2009 | History | 12 | 349 | 3487 | 0:37:41
2009 | Interview | 39 | 5445 | 58665 | 6:50:26
2009 | News | 155 | 4973 | 53846 | 6:56:17
2010 | Background | 126 | 19573 | 209609 | 23:06:04
2010 | Discovery | 78 | 3130 | 33133 | 3:29:00
2010 | History | 16 | 804 | 8328 | 0:57:25
2010 | Interview | 56 | 4162 | 43626 | 4:55:45
2010 | News | 468 | 20864 | 216025 | 24:50:38

#### How many videos had subtitles?

An analysis of the logs of the retrieval of the SRT files for each video is done with `check_which_videos_had_subtitles.py`.

An example of its usage can be found in `check_which_videos_had_subtitles.sh`.

This script finds all the Excel files which contain a log for every video found in EuroparlTV, their metadata found and the success of the download of the SRT subtitles, it returns a summary of how many videos the SRT file was downloaded (True) or not (False), by:

- all_videos: all videos for which we have a log, no grouping, the raw data to calculate the rest of aggregations
- lang: videos grouped by language
- year: videos grouped by language and year
- type: videos grouped by language and type
- category: videos grouped by language and category
- year_type: videos grouped by language, year and type
- year_category: videos grouped by language, year and category
- uri: videos grouped by an invariable portion of the URL (the same video has the same string in the URL for every language)
- lang_uri: videos grouped by language and the URI

#### How many files were processed?

An example of its usage can be found in `check_which_files_were_converted.sh`.
