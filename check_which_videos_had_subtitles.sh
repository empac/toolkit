#!/usr/bin/env bash

SCRIPT=./check_which_videos_had_subtitles.py

INDIR=/data/empac/corpus/SRT

OUTDIR=/data/empac/data

echo "Checking which videos had subtitles ...."
python3 $SCRIPT --input $INDIR --output $OUTDIR/empac_log_check.xlsx