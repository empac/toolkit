# -*- coding: utf-8 -*-

import os
import argparse
import re
import pandas as pd
import PyCQP_interface
from io import StringIO


class WrongSegmentationExtractor(object):
    """Extract wrong segmentations in two-lines subtitles from a corpus encoded for CWB."""

    def __init__(self, corpus_name=None, subcorpus=None, queries=None, normalization=None, outfile=None, outdir=None,
                 registry=None, cqp_bin=None):
        self.corpus_name = corpus_name
        self.subcorpus = subcorpus
        self.registry = registry
        self.cqp_bin = cqp_bin
        self.queries = queries
        self.normalization = normalization
        self.outdir = outdir
        self.outfile = outfile

        self.main()

    def parse_query_file(self, path_queries=None):
        """Parse a file containing a list of queries for CQPweb

        :param path_queries: the path to the queries file
        :type path_queries: str
        :return: a list of queries for CQP
        """
        with open(path_queries, mode='r', encoding='utf-8') as queries_file:
            lines = queries_file.readlines()
            query_list = list()
            query = dict()
            for line in lines:
                if line.startswith('#'):
                    feature, category = line.split('|')
                    query['feature'] = re.sub(r'# ', r'', feature).strip()
                    query['class'] = category.strip()
                else:
                    query['query'] = line.strip()
                    query_list.append(query)
                    query = dict()
        return query_list

    def get_subcorpora_dataframes(self, df=None, grouping=None):
        """Parse a file containing a list of queries for CQPweb

        :param path_queries: the path to the queries file
        :type path_queries: str
        :return: a list of queries for CQP
        """

        if self.subcorpus:
            # matrix: absolute feature frequencies across subcorpora
            subcorpus_grouping = grouping + [self.subcorpus]
            df_subcorpus = df.groupby(subcorpus_grouping).size().unstack(len(subcorpus_grouping) -1)
            # matrix: relative feature frequencies across subcorpora
            df_subcorpus_rel = pd.DataFrame()
            for subtotal in self.subtotals:
                df_subcorpus_rel[subtotal] = (df_subcorpus[subtotal] / self.subtotals[subtotal]) * 1000
            # globally
            df_total = pd.DataFrame()
            df_total['freq'] = df.groupby(grouping).size()
            df_total['total'] = self.total
            df_total['rel_freq'] = (df_total['freq'] / self.total) * 1000
            # absolute and relative feature frequencies for subcorpora in long format
            df_long = pd.DataFrame()
            df_long['freq'] = df.groupby(subcorpus_grouping).size()
            rel_freqs = list()
            sub_totals = list()
            for idx, row in df_long.iterrows():
                rel_freqs.append((row['freq'] / self.subtotals[idx[len(subcorpus_grouping)-1]]) * 1000)
                sub_totals.append(self.subtotals[idx[len(subcorpus_grouping)-1]])
            df_long['total'] = sub_totals
            df_long['rel_freq'] = rel_freqs
            dataframes = [df_total, df_long, df_subcorpus, df_subcorpus_rel]
        else:
            df['total'] = self.total
            df['rel_freq'] = (df['freq'] / self.total) * 1000
            dataframes = [df]
        return dataframes

    def serialize_subcorpora_dataframes(self, dataframes=None, suffix=None):
        # save results to excel
        if len(dataframes) == 4:
            dataframes[0].to_excel(self.writer, '{}_total'.format(suffix))
            dataframes[1].to_excel(self.writer, '{}_{}_long'.format(suffix, self.subcorpus))
            dataframes[2].to_excel(self.writer, '{}_{}_freq'.format(suffix, self.subcorpus))
            dataframes[3].to_excel(self.writer, '{}_{}_rel_freq'.format(suffix, self.subcorpus))
        elif len(dataframes) == 1:
            dataframes[0].to_excel(self.writer, '{}_total'.format(suffix), index=False)

    def main(self):
        # parse queries files
        query_list = self.parse_query_file(path_queries=self.queries)
        query_norm = self.parse_query_file(path_queries=self.normalization)[0]
        # start CQP session
        cqp = PyCQP_interface.CQP(bin=self.cqp_bin, options='-c -r {}'.format(self.registry))
        # set matching strategy
        cqp.Exec("set MatchingStrategy longest;")
        # choose corpus
        cqp.Exec(self.corpus_name + ";")
        # get the total number of hits for normalization
        cqp.Query(query_norm['query'])
        self.total = int(cqp.Exec('size Last;'))
        # get the subtotals for subcorpora
        if self.subcorpus:
            subtotal_tbl = StringIO(cqp.Exec('tabulate Last match text_{};'.format(self.subcorpus)))
            subtotal_df = pd.read_table(subtotal_tbl, header=None, names=[self.subcorpus])
            self.subtotals = subtotal_df.groupby(self.subcorpus).size().to_dict()
        # get all results in a DataFrame
        df = pd.DataFrame()
        # for each query in the queries file
        for idx, query in enumerate(query_list):
            cqp.Query(query['query'])
            if self.subcorpus:
                tbl = StringIO(cqp.Exec('tabulate Last match text_{};'.format(self.subcorpus)))
                tmp_df = pd.read_table(tbl, header=None, names=[self.subcorpus])
                tmp_df['class'] = query['class']
                tmp_df['feature'] = query['feature']
                df = df.append(tmp_df)
            else:
                result = {'class': query['class'],'feature': query['feature'], 'freq': int(cqp.Exec('size Last;'))}
                df = df.append(result, ignore_index=True)
        # close CQP session
        cqp.Terminate()
        # calculate frequencies
        if self.subcorpus:
            # by subcorpus
            feature_dfs = self.get_subcorpora_dataframes(df=df, grouping=['class', 'feature'])
            category_dfs = self.get_subcorpora_dataframes(df=df, grouping=['class'])
            # save results to excel
        else:
            # globally
            feature_dfs = self.get_subcorpora_dataframes(df=df, grouping=['class', 'feature'])
            category_dfs = self.get_subcorpora_dataframes(df=df, grouping=['class'])
        # save results to excel
        self.writer = pd.ExcelWriter(self.outfile)
        self.serialize_subcorpora_dataframes(dataframes=feature_dfs, suffix='feature')
        self.serialize_subcorpora_dataframes(dataframes=category_dfs, suffix='class')
        self.writer.save()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser = argparse.ArgumentParser()
    parser.add_argument("--corpus",
                        dest='corpus',
                        required=True,
                        help="name of the corpus as in CWB registry.")
    parser.add_argument("--subcorpus",
                        dest='subcorpus',
                        required=False,
                        help="name of the variable to calculate subcorpus frequencies.")
    parser.add_argument("--output",
                        dest='output',
                        required=True,
                        help="path to the file to save the output.")
    parser.add_argument("--queries",
                        dest='queries',
                        required=True,
                        help="list of queries to find wrong segmentations.")
    parser.add_argument("--normalization",
                        dest='normalization',
                        required=True,
                        help="query to get the number of two line subtitles \
                                in corpus for normalization purposes.")
    parser.add_argument("--registry",
                        dest='registry',
                        required=False,
                        default='~/CORPORA/registry',
                        help="path to CWB registry."
                        )
    parser.add_argument("--cqp",
                        dest="cqp",
                        required=False,
                        default='cqp',
                        help="path to the `cqp` binary."
                        )

    args = parser.parse_args()

    corpus_name = args.corpus
    subcorpus = args.subcorpus
    registry = args.registry
    cqp_bin = args.cqp
    queries = args.queries
    normalization = args.normalization
    outfile = args.output
    outdir = os.path.split(outfile)[0]
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    WrongSegmentationExtractor(corpus_name=corpus_name, subcorpus=subcorpus, queries=queries,
                               normalization=normalization, outfile=outfile, outdir=outdir,
                               registry=registry, cqp_bin=cqp_bin)