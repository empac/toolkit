#!/usr/bin/env bash

YEARS=(
2009
2010
2011
2012
2013
2014
2015
2016
2017
)

LANGUAGES=(
en
es
)

## Programme types
TYPES=(
1 # News
2 # Interview
3 # Background
4 # Discovery
5 # History
6 # Others
)

INDIR=/data/empac/corpus/SRT

OUTDIR=/data/empac/corpus/XML

SCRIPT=./from_srt_to_xml.py

for language in ${LANGUAGES[@]}
do
    for year in ${YEARS[@]}
    do
        for t in ${TYPES[@]}
        do
            echo "Transforming SRT to XML for $language $year $t ...."
            python3 $SCRIPT --input $INDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --output $OUTDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --glob "*.srt" --metadata $INDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t/metadata.xlsx --lang $language
        done
    done
done