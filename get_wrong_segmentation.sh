#!/usr/bin/env bash

LANGUAGES=(
en
es
)

SCRIPT=./get_wrong_segmentation.py

INDIR=./

OUTDIR=/data/empac/data

SUBCORPORA=(
year
category
type
)

for language in ${LANGUAGES[@]}
do
    for subcorpus in ${SUBCORPORA[@]}
    do
        echo "Getting wrong segmentations for $language $subcorpus ...."
        python3 $SCRIPT --corpus EMPAC_$(echo $language | tr "[:lower:]" "[:upper:]") --subcorpus $subcorpus --output $OUTDIR/queries_wrong_segmentation_intra_$language\_$subcorpus.xlsx --queries $INDIR/queries_wrong_segmentation_intra_$language.txt --normalization $INDIR/query_normalization_intra.txt --registry /data/cqpweb/registry
        python3 $SCRIPT --corpus EMPAC_$(echo $language | tr "[:lower:]" "[:upper:]") --subcorpus $subcorpus --output $OUTDIR/queries_wrong_segmentation_inter_$language\_$subcorpus.xlsx --queries $INDIR/queries_wrong_segmentation_inter_$language.txt --normalization $INDIR/query_normalization_inter.txt --registry /data/cqpweb/registry
     done
done