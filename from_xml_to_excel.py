import argparse
import os
import fnmatch
from lxml import etree
import pandas as pd
from tqdm import tqdm

def get_files(directory, fileclue):
    """Get all files in a directory matching a pattern.

    Keyword arguments:
    directory -- a string for the input folder path
    fileclue -- a string as glob pattern
    """
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

def read_xml(infile):
    """Parse a HTML file."""
    with open(infile, encoding='utf-8', mode='r') as input:
        xml = etree.parse(input)
    return xml

def format_text_df(text_df):
    text_df = text_df.copy(deep=True)
    text_cols = [
        'id',
        'record',
        'version',
        'lang',
        'type',
        'category',
        'year',
        'month',
        'day',
        'date',
        'duration',
        'subtitles',
        'n_lines',
        'one_liners',
        'two_liners',
        'n_liners',
        'tokens',
        'video_url',
        'srt_url',
        'description'
    ]
    text_df = text_df[text_cols]
    text_df['subtitles'] = text_df['subtitles'].astype('int64')
    text_df['n_lines'] = text_df['n_lines'].astype('int64')
    text_df['one_liners'] = text_df['one_liners'].astype('int64')
    text_df['two_liners'] = text_df['two_liners'].astype('int64')
    text_df['n_liners'] = text_df['n_liners'].astype('int64')
    text_df['tokens'] = text_df['tokens'].astype('int64')
    text_df['type'] = text_df['type'].astype('category')
    text_df['category'] = text_df['category'].astype('category')
    text_df['date'] = text_df['date'].astype('datetime64[ns]')
    text_df.rename(columns={'id': 'text_id'}, inplace=True)
    return text_df


def get_metadata(element):
    metadata = pd.Series({k: v for k, v in element.attrib.items()})
    return metadata

def get_subtitles(element):
    subtitles = element.xpath('//subtitle')
    subtitles_metadata = [{k: v for k, v in x.items()} for x in subtitles]
    subtitles_df = pd.DataFrame(subtitles_metadata)
    subtitles_df['text_id'] = element.attrib['id']
    sub_cols = ['text_id', 'id', 'no', 'begin', 'end', 'duration', 'chars', 'cps', 'n_lines', 'tokens', 'pause']
    subtitles_df = subtitles_df[sub_cols]
    subtitles_df['duration'] = subtitles_df['duration'].astype('float64').round(3)
    subtitles_df['n_lines'] = subtitles_df['n_lines'].astype('int64')
    subtitles_df['tokens'] = subtitles_df['tokens'].astype('int64')
    subtitles_df['chars'] = subtitles_df['chars'].astype('int64')
    subtitles_df['cps'] = subtitles_df['cps'].astype('float64').round(2)
    subtitles_df['no'] = subtitles_df['no'].astype('int64')
    subtitles_df['pause'] = subtitles_df['pause'].astype('float64').round(3)
    subtitles_df.rename(columns={'id': 'subtitle_id'}, inplace=True)
    return subtitles_df

def get_lines(element):
    lines = element.xpath('//line')
    lines_metadata = [{k: v for k, v in x.items()} for x in lines]
    lines_df = pd.DataFrame(lines_metadata)
    lines_df['text_id'] = element.attrib['id']
    line_ids = lines_df['id'].tolist()
    sub_ids = [element.xpath('//line[@id="{}"]'.format(x))[0].getparent().attrib['id'] for x in line_ids]
    lines_df['subtitle_id'] = sub_ids
    lines_cols = ['text_id', 'subtitle_id', 'id', 'no', 'chars', 'tokens']
    lines_df = lines_df[lines_cols]
    lines_df['no'] = lines_df['no'].astype('int64')
    lines_df['chars'] = lines_df['chars'].astype('int64')
    lines_df['tokens'] = lines_df['tokens'].astype('int64')
    lines_df.rename(columns={'id': 'line_id'}, inplace=True)
    return lines_df

def run_from_xml_to_excel(indir=None, outfile=None, glob_pattern=None):
    infiles = get_files(indir, glob_pattern)
    sub_df = pd.DataFrame()
    text_df = pd.DataFrame()
    lines_df = pd.DataFrame()
    for f in tqdm(infiles):
        tree = read_xml(f)
        root = tree.getroot()
        text_metadata = get_metadata(root)
        text_df = text_df.append(text_metadata, ignore_index=True)
        subtitles = get_subtitles(root)
        sub_df = sub_df.append(subtitles)
        lines = get_lines(root)
        lines_df = lines_df.append(lines)
    text_df = format_text_df(text_df)
    outdir, outfname = os.path.split(outfile)
    outfnamebase, outfnameext = os.path.splitext(outfname)
    outpath_text = os.path.join(outdir, '{}_text{}'.format(outfnamebase, outfnameext))
    text_df.to_excel(outpath_text, index=False)
    sub_lines = pd.merge(sub_df, lines_df, on=['text_id', 'subtitle_id'], how='right', suffixes=('_s', '_l'))
    all_df = pd.merge(text_df, sub_lines, on=['text_id'], how='right', suffixes=('_t', '_s'))
    sub_df = pd.merge(text_df, sub_df, on=['text_id'], how='right', suffixes=('_t', '_s'))
    writer = pd.ExcelWriter(outfile, engine='xlsxwriter', options={'strings_to_urls': False})
    text_df.to_excel(writer, sheet_name="texts", index=False)
    sub_df.to_excel(writer, sheet_name="subtitles", index=False)
    # lines_df.to_excel(writer, sheet_name="lines", index=False)
    all_df.to_excel(writer, sheet_name="lines", index=False)
    writer.save()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='indir',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--output",
        dest='outfile',
        required=True,
        help="path to the output directory.")

    parser.add_argument(
        "--pattern",
        dest='pattern',
        required=False,
        default="*.xml",
        help="glob pattern to filter files.")

    args = parser.parse_args()

    indir = args.indir
    outfile = args.outfile
    glob_pattern = args.pattern

    outdir, out_fname = os.path.split(outfile)

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    run_from_xml_to_excel(indir=indir, outfile=outfile, glob_pattern=glob_pattern)