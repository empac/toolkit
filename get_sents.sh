#!/usr/bin/env bash

LANGUAGES=(
en
es
)

YEARS=(
2009
2010
2011
2012
2013
2014
2015
2016
2017
)

## Programme types
TYPES=(
1 # News
2 # Interview
3 # Background
4 # Discovery
5 # History
6 # Others
)

SCRIPT=./get_sentences.py

INDIR=/data/empac/corpus/VRT

OUTDIR=/data/empac/corpus/SENTS

for language in ${LANGUAGES[@]}
do
    for year in ${YEARS[@]}
    do
        for t in ${TYPES[@]}
        do
            echo "Adding sentences for $language $year $t ...."
            python3 $SCRIPT --input $INDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --output $OUTDIR/$(echo $language | tr "[:lower:]" "[:upper:]")/$year/$t --lang $language
        done
    done
done