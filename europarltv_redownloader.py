import argparse
import os
import requests
import pandas as pd
import fnmatch  # To match files by pattern


programme_types = {
    'News': '1',
    'Interview': '2',
    'Background': '3',
    'Discovery': '4',
    'History': '5',
    'Others': '6'
}

categories = {
    'en': {
        'EU affairs': 'EU affairs',
        'Economy': 'Economy',
        'Security': 'Security',
        'Society': 'Society',
        'World': 'World',
        'Others': 'Others'
    },
    'es': {
        'Asuntos de la UE': 'EU affairs',
        'Economía': 'Economy',
        'Seguridad': 'Security',
        'Sociedad': 'Society',
        'Mundo': 'World',
        'Others': 'Others'
    },
    'de': {
        'EU-Angelegenheiten': 'EU affairs',
        'Wirtschaft': 'Economy',
        'Sicherheit': 'Security',
        'Gesellschaft': 'Society',
        'Welt': 'World',
        'Others': 'Others'
    }
}

def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

def download_subtitles(indir=None, outdir=None, glob_pattern=None, resume=None):
    input_files = get_files(indir, glob_pattern)
    for i in input_files:
        metadata = list()
        print(i)
        df = pd.read_excel(i)
        hits = len(df)
        print("Total number of hits: {}".format(hits))
        ifpath = splitall(i)
        lang = ifpath[-4]
        year = ifpath[-3]
        try:
            programme_type = programme_types[ifpath[-2]]
        except:
            programme_type = ifpath[-2]
        ofdir = os.path.join(outdir, 'SRT', lang, year, programme_type)
        if not os.path.exists(ofdir):
            os.makedirs(ofdir)
        for idx, row in df.iterrows():
            srtbasename = os.path.basename(row['srt_url'])
            print(row['srt_url'])
            print(row['video_url'])
            if srtbasename != 'null.srt':
                try:
                    row['category'] = categories[row['lang']][row['category']]
                except:
                    row['category'] = row['category']
                print(row['id'])
                subtitles = requests.get(row['srt_url'])
                if subtitles.status_code == requests.codes.ok:
                    ofbasename = '{}.srt'.format(row['id'])
                    ofpath = os.path.join(ofdir, ofbasename)
                    print(ofpath)
                    with open(ofpath, mode='wb') as osrt:
                        osrt.write(subtitles.content)
                    metadata.append(row)
            else:
                print("File not processed!= {}: {}".format(row['lang'], os.path.basename(row['srt_url'])))
                metadata.append(row)
                continue
        print(metadata)
        metadata = pd.DataFrame(metadata)
        print(metadata)
        mfname = "metadata.xlsx"
        mfpath = os.path.join(ofdir, mfname)
        metadata.to_excel(mfpath, index=False)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='input',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output directory."
    )

    parser.add_argument(
        "--glob",
        dest="glob_pattern",
        required=False,
        help="glob pattern to select input files."
    )

    parser.add_argument(
        "--resume",
        dest="resume",
        action="store_true",
        help="resume download."
    )

    args = parser.parse_args()

    indir = args.input
    outdir = args.output
    glob_pattern = args.glob_pattern
    resume = args.resume

    download_subtitles(
        indir=indir,
        outdir=outdir,
        glob_pattern=glob_pattern,
        resume=resume
    )