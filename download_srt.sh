#!/usr/bin/env bash

## Languages
LANGUAGES=(
en
es
)

## Programme types
TYPES=(
1
2
3
4
5
6
)

## Years
YEARS=(
2009
2010
2011
2012
2013
2014
2015
2016
2017
)

## Output directory
OUT=/data/empac/corpus

## Path to script
SCRIPT=./europarltv_subtitle_downloader.py

## process each corpus
for language in ${LANGUAGES[@]}
do
    for year in ${YEARS[@]}
    do
        for t in ${TYPES[@]}
        do
            echo $language $year $t
            python3 $SCRIPT --from 1/1/$year --to 1/1/$((year + 1)) --lang $language --type $t --output $OUT/$language/$year/$t
        done
    done
done