import argparse
import os
import pandas as pd


def summarize_groups(in_df, grouping_var):
    """Summarize groups for a given variable in a DataFrame

    :param in_df: input DataFrame
    :type in_df: pd.DataFrame
    :param grouping_var: the name of the column/variable to group the data
    :type grouping_var: str
    :return: a DataFrame
    """
    df = pd.DataFrame()
    df['texts'] = in_df.groupby(grouping_var).size()
    for i in ['subtitles', 'tokens', 'duration']:
        filtering = grouping_var + [i]
        df[i] = in_df[filtering].groupby(grouping_var).sum()
        if i == 'duration':
            df[i] = df[i].apply(duration_to_hours)
    return df


def duration_to_hours(x):
    """Convert time delta for duration to hours.

    :param x: a time delta object
    :return: the duration in hours
    :rtype: str
    """
    components = x.components
    output = "{0:d}:{1:02d}:{2:02d}".format(
        (components[0] * 24) + components[1],
        components[2],
        components[3]
    )
    return output


def run_corpus_description(infile=None, outfile=None):
    """Describe a corpus from the data in an Excel file.

    :param infile: the path to the Excel input file
    :type infile: str
    :param outfile: the path to the Excel output file
    :type outfile: str
    """
    in_df = pd.read_excel(infile)
    lang_df = pd.DataFrame()
    in_df['duration'] = pd.to_timedelta(in_df['duration'], errors='coerce')
    lang_df['texts'] = [in_df.text_id.count()]
    lang_df['subtitles'] = [in_df.subtitles.sum()]
    lang_df['tokens'] = [in_df.tokens.sum()]
    lang_df['duration'] = [duration_to_hours(in_df['duration'].sum())]
    year_df = summarize_groups(in_df, ['year'])
    type_df = summarize_groups(in_df, ['type'])
    category_df = summarize_groups(in_df, ['category'])
    year_type_df = summarize_groups(in_df, ['year', 'type'])
    year_category_df = summarize_groups(in_df, ['year', 'category'])
    writer = pd.ExcelWriter(outfile, engine='xlsxwriter', options={'strings_to_urls': False})
    lang_df.to_excel(writer, sheet_name="total", index=False)
    year_df.to_excel(writer, sheet_name='year')
    type_df.to_excel(writer, sheet_name='type')
    category_df.to_excel(writer, sheet_name='category')
    year_type_df.to_excel(writer, sheet_name='year_type')
    year_category_df.to_excel(writer, sheet_name='year_category')
    writer.save()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='infile',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--output",
        dest='outfile',
        required=True,
        help="path to the output directory.")

    args = parser.parse_args()

    inputfile = args.infile
    outfile = args.outfile

    outdir, out_fname = os.path.split(outfile)

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    run_corpus_description(infile=inputfile, outfile=outfile)