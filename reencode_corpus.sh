#!/usr/bin/env bash

## directory where the registry is located
REGISTRY=/data/cqpweb/registry

LANG=(
EN
ES
)

## VRT directory
VRT=/data/empac/corpus/VRT

## TMP VRT directory
TMP=/data/empac/vrt

## SENTS directory
SENTS=/data/empac/corpus/SENTS

## SENTS directory
TMP_SENTS=/data/empac/vrt_sents

## Alignments script
ALIGNER=./get_alignments.py

## Output directory for alignment
OUTALIGN=/data/empac/corpus

## Output directory for indexed corpora
OUT=/data/cqpweb/indexed

## Log directory
LOG=/data/empac/data

## CWB directory
CWB=/usr/local/cwb-3.4.15/bin

## CWB Perl
CWBPERL=/home/ubuntu/perl5/bin

## CWB

for l in ${LANG[@]}
do
    echo "Reencoding EMPAC $l for CWB ...."
    echo "Creating tmp directory for VRT at $TMP/$l ...."
    mkdir -p $TMP/$l
    echo "Copying VRT files from $VRT to $TMP/$l ...."
    for i in $(find $VRT/$l -type f -name "*.vrt")
    do
        cp $i $TMP/$l/${i##*/}
    done
    echo "Creating tmp directory for SENTS at $TMP_SENTS/$l ...."
    mkdir -p $TMP_SENTS/$l
    echo "Copying VRT files from $SENTS/$l to $TMP_SENTS/$l ...."
    for i in $(find $SENTS/$l -type f -name "*.vrt")
    do
        cp $i $TMP_SENTS/$l/${i##*/}
    done
    CNAME=EMPAC_$l
    echo "Corpus name: $CNAME"
    RNAME=$(echo $CNAME | tr "[:upper:]" "[:lower:]")
    echo "Registry name: $RNAME"
    OUTINDEXED=$OUT/EMPAC/$l
    echo "Output directory: $OUT"
    mkdir -p $OUTINDEXED
    echo "Cleaning previous data from: $REGISTRY/$RNAME and $OUT ...."
    ## clean up previous encoded data
    rm $REGISTRY/$RNAME
    ## clean up previous encoded data
    rm $OUTINDEXED/*
    echo "Encoding corpus $CNAME ...."
    ## encode text metadata, p, s, word, tok, pos, lemma
    $CWB/cwb-encode -c utf8 -d $OUTINDEXED -F $TMP/$l -R $REGISTRY/$RNAME -xsB -S text:0+id+record+version+lang+type+category+year+month+day+date+duration+title+subtitles+n_lines+one_liners+two_liners+n_liners+tokens+video_url+srt_url+description -S subtitle:0+id+no+begin+end+duration+chars+cps+n_lines+tokens+pause -S line:0+id+no+chars+tokens -P pos -P lemma
    ## finish corpus compilation
    $CWBPERL/cwb-make -r $REGISTRY -V $CNAME
    echo "Adding sentence annotation to $CNAME ...."
    ## add s
    $CWB/cwb-encode -c utf8 -d $OUTINDEXED -F $TMP_SENTS/$l -xsB -p - -0 text -S s:0+id+no+tokens
    ## declare s attribute
    $CWBPERL/cwb-regedit -r $REGISTRY $CNAME :add :s s s_id s_no s_tokens
    # ## rm TMP
    # echo "Removing TMP data from $TMP/$l and $TMP_SENTS/$l"
    # rm -r $TMP/$l
    # rm -r $TMP_SENTS/$l
done

echo "Aligning EMPAC_EN with EMPAC_ES ...."
## generate alignment en-es
python3 $ALIGNER --v1 $TMP/EN --v2 $TMP/ES --output $OUTALIGN/en_es_alignment.txt --log $LOG/en_es_alignment_log.xlsx --glob "*.vrt"

# add alignments en-es
$CWBPERL/cwb-align-import -p -r $REGISTRY -e -nh -l1 EMPAC_EN -l2 EMPAC_ES -s subtitle -k '{text_id}:{subtitle_no}' $OUTALIGN/en_es_alignment.txt

$CWBPERL/cwb-align-import -i -p -r $REGISTRY -e -nh -l1 EMPAC_EN -l2 EMPAC_ES -s subtitle -k '{text_id}:{subtitle_no}' $OUTALIGN/en_es_alignment.txt