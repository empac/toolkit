import os
import argparse
import re  # to use regular expressions
import datetime
import fnmatch  # To match files by pattern
import logging
import pandas as pd
import hashlib
from unidecode import unidecode
from tqdm import tqdm
from lxml import etree


logger = logging.getLogger('srt2xml')
logger.setLevel(logging.INFO)

def transform_srt_to_xml(indir=None, outdir=None, metadata=None, lang=None, glob_pattern=None):
    """Transform files from SRT format to XML.

    :param indir: input directory for SRT files
    :type indir: str
    :param outdir: output directory to save XML files
    :type outdir: str
    :param metadata: path to metadata file
    :type metadata: str
    :param lang: 2-letter language ISO code
    :type lang: str
    :param glob_pattern: glob pattern to filter files
    :type glob_pattern: str
    """
    if os.path.isdir(indir):
        input_files = get_files(indir, glob_pattern)
        meta = pd.read_excel(metadata, index=False)
        if len(input_files) > 0:
            for i in tqdm(input_files):
                # check that the language of the file is right
                if re.match(r'.+?(-{}X[-_].*|-{}\d*[-_].*|-{}\d*|-{}[A-Z]+)\.srt$'.format(*[lang.upper()] * 4), os.path.basename(i)):
                    logger.info(i)
                    subtitles = parse_subtitles(i)
                    if subtitles:
                        xml = generate_xml(subtitles, meta, i)
                        filename = os.path.splitext(os.path.basename(i))[0]
                        ofilepath = os.path.join(outdir, filename + '.xml')
                        with open(ofilepath, mode='w', encoding='utf-8') as ofile:
                            ofile.write(xml)
                    else:
                        logger.warning("File not processed because no subtitles found")
                else:
                    logger.warning("File not processed because language != {}: {}".format(lang.upper(), i))
        else:
            logger.warning("No SRT files found")
    else:
        logger.warning("Input directory '{}' not found".format(indir))

def get_files(directory, fileclue):
    matches = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, fileclue):
            matches.append(os.path.join(root, filename))
    return matches

def parse_time(tinput):
    hours, minutes, seconds = tinput.split(':')
    seconds, microseconds = seconds.split(',')
    microseconds = microseconds + 1 * '0'
    toutput = datetime.time(int(hours), int(minutes), int(seconds), int(microseconds))
    return toutput

def get_duration(intime, outtime):
    """Get the duration of a period of time comprised between two times.

    :param intime: beginning
    :type intime: str
    :param outtime: end
    :type outtime: str
    :return: the duration
    :rtype: str
    """
    dummydate = datetime.date(2000, 1, 1)
    intime = parse_time(intime)
    outtime = parse_time(outtime)
    duration = datetime.datetime.combine(dummydate, outtime) - datetime.datetime.combine(dummydate, intime)
    duration = duration.total_seconds()
    output = '{:.3f}'.format(duration)
    return output

def get_sub_length(lines):
    """Get subtitle and line lengths

    :param lines: the lines of a subtitle
    :type lines: list of str
    :return: number of lines, length in characters of the subtitles, the lines with their length in characters
    :rtype: str, str, list
    """
    def clean_line(line):
        line = re.sub(r'</?i>', r'', line)
        return line

    sub_length = 0
    for idx, line in enumerate(lines):
        if len(line) == 0:
            del lines[idx]
        else:
            sub = clean_line(line)
            line_length = len(sub)
            sub_length += len(sub)
            lines[idx] = {'chars': str(line_length), 'text': line}
    n_lines = str(len(lines))
    sub_length= str(sub_length)
    if n_lines == '0':
        return None
    else:
        return n_lines, str(sub_length), lines

def process_italics(lines):
    result = []
    for line in lines:
        line = re.sub(r'\{\\i1\}(.+?)\{\\i0\}', r'<i>\1</i>', line)
        line = re.sub(r'\{\\i1\}(.+)', r'<i>\1</i>', line)
        line = re.sub(r'(.+)\{\\i0\}', r'<i>\1</i>', line)
        result.append(line)
    return result

def remove_format(lines):
    result = []
    for line in lines:
        line = re.sub(r'\{.+?}', r'', line)
        result.append(line)
    return result

def parse_subtitles(infile):
    """Parse SRT file.

    :param infile: path to the file
    :type infile: str
    :return:
    """
    with open(infile, mode='r', encoding='utf-8') as input:
        content = input.read()
    subtitles = list()
    content = re.sub(r'\r\n', r'\n', content)  # normalize end of lines
    segments = re.findall(  # get a list of all subtitles
        r'(\d+)\n{1,2}(\d\d:\d\d:\d\d,\d\d\d) --> (\d\d:\d\d:\d\d,\d\d\d)\n(.+?)\n{1,2}(?=\d+\n{1,2}\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d)', content,
        flags=re.DOTALL)
    last_segment = re.findall(
        r'(\d+)\n{1,2}(\d\d:\d\d:\d\d,\d\d\d) --> (\d\d:\d\d:\d\d,\d\d\d)\n((.+?\n){1,3})\n*$',
        content)
    if last_segment:
        segments.append(last_segment[0][:4])
    if len(segments) > 0:
        for idx, segment in enumerate(segments):  # model subtitles
            sub = dict()
            sub['no'] = segment[0]
            sub['begin'] = segment[1]
            sub['end'] = segment[2]
            sub['lines'] = [re.sub(r'##@#', r'', x.strip()) for x in filter(None, segment[3].split('\n'))]
            sub['duration'] = get_duration(sub['begin'], sub['end'])
            # sub['lines'] = process_italics(sub['lines'])
            # sub['lines'] = remove_format(subtitle)
            sub['n_lines'], sub['chars'], sub['lines'] = get_sub_length(sub['lines'])
            try:
                sub['cps'] = '{:.2f}'.format(int(sub['chars'])/float(sub['duration']))
            except ZeroDivisionError:
                logger.warning('Duration of subtitle == 0')
            #     sub['cps'] = '{:.2f}'.format(0)
            if idx == 0:
                sub['pause'] = str(0)
            else:
                sub['pause'] = get_duration(subtitles[idx-1]['end'], sub['begin'])
            subtitles.append(sub)
        if len(subtitles) != int(subtitles[-1]['no']):  # verify that no subtitle was lost
            logger.error("Subtitle number mistmatch ({} != {}) in {}".format(len(subtitles), subtitles[-1]['no'], infile))
    else:
        logger.error("No subtitles in {}".format(infile))
        subtitles = None
    return subtitles

def string_to_handle(s):
    s = unidecode(s)
    s = re.sub(r'[^\w\d_]+', r'_', s)
    return s

def generate_xml(subtitles, metadata, infile):
    """Generate XML file from parsed subtitles and metadata.

    :param subtitles: parsed subtitles
    :type subtitles: list
    :param metadata: metadata for all files
    :type metadata: DataFrame
    :param infile: path to theinput file
    :type infile: str
    :return: the XML file
    :rtype: str
    """
    filename = os.path.splitext(os.path.basename(infile))[0]
    filemeta = metadata[metadata['id'] == filename].squeeze()
    filemeta['id'] = string_to_handle(filemeta['id'])
    filemeta['category'] = string_to_handle(filemeta['category'])
    filemeta['type'] = string_to_handle(filemeta['type'])
    fn_match = re.match(r'(.+)-({}.*)$'.format(lang.upper()), filename)
    if fn_match:
        filemeta['record'] = fn_match.group(1)
        filemeta['version'] = fn_match.group(2)
    else:
        logger.warning("Bad filename!")
        filesplit = filename.split('-')
        if len(filesplit) == 3:
            filemeta['record'] = "_".join(filesplit[:2])
        elif len(filesplit) > 3:
            filemeta['record'] = "_".join(filesplit[:3])
        filemeta['version'] = "_".join(filename.split('-')[3:])
    date, hour = filemeta['date'].split(' - ')
    hours, minutes = hour.split(":")
    filemeta['day'], filemeta['month'], filemeta['year'] = date.split('.')
    filemeta['date'] = str(datetime.datetime(int(filemeta['year']), int(filemeta['month']), int(filemeta['day']), int(hours), int(minutes)))
    filemeta['subtitles'] = str(len(subtitles))
    filemeta.rename({'length': 'duration'}, inplace=True)
    try:  # use duration of the video from metadata
        min, sec = filemeta['duration'].split(':')
        filemeta['duration'] = str(datetime.time(0, int(min), int(sec)))
    except:  # if info not provided, then get it from first and last subtitle
        filemeta['duration'] = get_duration(subtitles[0]['begin'], subtitles[-1]['end'])
    filemeta['one_liners'] = str(sum([1 for x in subtitles if x['n_lines'] == '1']))
    filemeta['two_liners'] = str(sum([1 for x in subtitles if x['n_lines'] == '2']))
    filemeta['n_liners'] = str(sum([1 for x in subtitles if int(x['n_lines']) > 2]))
    filemeta['n_lines'] = str(sum([int(x['n_lines']) for x in subtitles]))
    root = etree.Element("text", attrib=filemeta.to_dict())
    for i in subtitles:
        submeta = {k: v for k, v in i.items() if k in ['no', 'begin', 'end', 'duration', 'n_lines', 'chars', 'cps', 'pause']}
        submeta['id'] = hashlib.md5("_".join((filemeta['id'], i['no'])).encode()).hexdigest()
        subtitle = etree.SubElement(root, 'subtitle', attrib=submeta)
        for idx, line in enumerate(i['lines']):
            linemeta = {k: v for k, v in line.items() if k in ['chars']}
            linemeta['no'] = str(idx+1)
            linemeta['id'] = hashlib.md5("_".join((filemeta['id'], submeta['no'], linemeta['no'])).encode()).hexdigest()
            etree.SubElement(subtitle, 'line', attrib=linemeta).text = line['text']
    content = etree.tostring(root, encoding='utf-8', method='xml', pretty_print=True, xml_declaration=True).decode()
    # content = re.sub(r'&lt;i&gt;', r'<i>', content)
    # content = re.sub(r'&lt;/i&gt;', r'</i>', content)
    return content

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        dest='input',
        required=True,
        help="path to the input directory.")

    parser.add_argument(
        "--metadata",
        dest='metadata',
        required=False,
        default="",
        help="path to metadata file."
    )

    parser.add_argument(
        "--output",
        dest='output',
        required=True,
        help="path to the output directory."
    )

    parser.add_argument(
        "--lang",
        dest='lang',
        required=True,
        help="language in two-letter ISO code format."
    )

    parser.add_argument(
        "--glob",
        dest="glob_pattern",
        required=False,
        help="glob pattern to select input files."
    )

    args = parser.parse_args()

    indir = args.input
    metadata = args.metadata
    outdir = args.output
    lang = args.lang
    glob_pattern = args.glob_pattern

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    transform_srt_to_xml(
        indir=indir,
        outdir=outdir,
        metadata=metadata,
        lang=args.lang,
        glob_pattern=glob_pattern
    )