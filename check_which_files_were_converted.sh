#!/usr/bin/env bash

LANGUAGES=(
en
es
)

SCRIPT=./compare_files_in_directories.py

INDIR=/data/empac/corpus

OUTDIR=/data/empac/data

for language in ${LANGUAGES[@]}
do
    echo "Checking which files in $language were converted from SRT to XML ...."
    python3 $SCRIPT --dir1 $INDIR/SRT/$(echo $language | tr "[:lower:]" "[:upper:]") --dir2 $INDIR/XML/$(echo $language | tr "[:lower:]" "[:upper:]") --output $OUTDIR/diff_files_srt_xml_$language.xlsx --glob1 "*.srt" --glob2 "*.xml"
    echo "Checking which files in $language were converted from SRT to VRT ...."
    python3 $SCRIPT --dir1 $INDIR/SRT/$(echo $language | tr "[:lower:]" "[:upper:]") --dir2 $INDIR/VRT/$(echo $language | tr "[:lower:]" "[:upper:]") --output $OUTDIR/diff_files_srt_vrt_$language.xlsx --glob1 "*.srt" --glob2 "*.vrt"
    echo "Checking which files in $language were converted from SRT to TMP ...."
    python3 $SCRIPT --dir1 $INDIR/SRT/$(echo $language | tr "[:lower:]" "[:upper:]") --dir2 $INDIR/TMP/$(echo $language | tr "[:lower:]" "[:upper:]") --output $OUTDIR/diff_files_srt_tmp_$language.xlsx --glob1 "*.srt" --glob2 "*.vrt"
done